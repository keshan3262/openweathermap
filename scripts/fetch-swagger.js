const fs = require('fs');
const fetch = require('node-fetch');

const url = 'http://localhost:3000/api-docs.json';
const outputFile = 'src/swagger/api-docs.json';

fetch(url)
  .then(response => response.json())
  .then(body => fs.writeFileSync(outputFile, JSON.stringify(body)))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
