export SOURCE=./api_source
export DESTINATION=src/swagger
mkdir -p $SOURCE
echo "Generating typescript code..."
java -jar swagger-codegen-cli.jar generate -l typescript-node \
-DsupportsES6=true -i src/swagger/api-docs.json -o $SOURCE
echo "Copying..."
cp $SOURCE/*.ts $DESTINATION
rm -rf $SOURCE
sed -e "s/https:/http:/" $DESTINATION/api.ts > $DESTINATION/api.ts.tmp
mv $DESTINATION/api.ts.tmp $DESTINATION/api.ts
