const path = require('path');
const workingDirPath = path.resolve('../', __dirname);
const git = require('simple-git/promise')(workingDirPath);
const process = require('process');
const chalk = require('chalk');

(async () => {
  const { files } = await git.status();

  if (files.length) {
    console.error(chalk.bold.red('\n ❌ Git root is dirty 🤔 ❌ \n'));
    console.error(chalk.red('Changed files : '));
    files.forEach(({ path }) => {
      console.error(chalk.red(`\t${path}`));
    });
    console.error(
      chalk.red(`\n-------------    GIT DIFF DUMP START -------------`)
    );
    console.log(await git.diff());
    console.error(
      chalk.red(`\n-------------    GIT DIFF DUMP END -------------`)
    );
    process.exit(1);
  } else {
    console.log('\n ✅ Git working directory is clean 👍 ✅ \n');
  }
})();
