import { configure } from '@storybook/react';
import moment from 'moment-timezone';
import requireContext from 'require-context.macro';
import 'typeface-roboto';
import '../src/utils/mock-router';

// Stories output should be independent on timezone of the environment
moment.tz.setDefault('Greenwich');

function loadStories() {
  const req = requireContext('../src', true, /.stories.tsx$/);
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);