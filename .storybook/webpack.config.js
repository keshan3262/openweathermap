const path = require('path');
const webpack = require('webpack');
const process = require('process');

function applyStorybookImageMocks(config) {
  // additional components mocks can be added here
}

module.exports = function({ config, mode }) {
  config.plugins.push(
    new webpack.IgnorePlugin(
      /\/(?:__image_snapshots__)|(?:__snapshots__)|(?:__diff_output__)\//
    ),
    new webpack.WatchIgnorePlugin([
      /__image_snapshots__/,
      /__snapshots__/,
      /__diff_output__/
    ]),
    new webpack.NamedModulesPlugin()
  );

  if (!config.optimization) {
    config.optimization = {};
  }

  if (!config.optimization.minimizer) {
    config.optimization.minimizer = [];
  }

  if (!config.module.rules) {
    config.module.rules = [];
  }

  const eslintRule = config.module.rules.find(({ test }) => test && test.test('.ts'));

  if (eslintRule) {
    const pluginToModify = eslintRule.use.find(({ options }) => options.useEslintrc === false);
    pluginToModify.options.useEslintrc = true;
  }

  if (!config.watchOptions) config.watchOptions = {};
  config.watchOptions.ignored = /\/(?:__image_snapshots__)|(?:__snapshots__)|(?:__diff_output__)\//;

  if (!config.resolve.plugins) config.resolve.plugins = [];

  if (!config.resolve.alias) {
    config.resolve.alias = {};
  }

  if (process.env.STORYSHOT_IMAGE) {
    console.log('APPLY STORYSHOT_IMAGE MODE');
    applyStorybookImageMocks(config);
  }

  config.resolve.modules = [path.join(__dirname, '../src/'), 'node_modules'];

  if (typeof config.node !== 'object') {
    config.node = {};
  }
  Object.assign(config.node, {
    net: 'mock',
    tls: 'mock'
  });

  return config;
};
