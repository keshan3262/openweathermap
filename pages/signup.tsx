import { NextPage } from 'next';
import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  SignUpCredentials,
  SignUpPageLayout,
  SignUpPageLayoutProps
} from '../src/layouts/SignUpPageLayout';
import { setError } from '../src/redux/error/actions';
import { RootState } from '../src/redux/store';
import { LoginControllerApi } from '../src/swagger/api';
import { signIn } from '../src/utils/auth';

type SignUpPageProps = {
  onSubmit: (values: SignUpCredentials) => void;
};

export const config = { amp: 'hybrid' };

const SignUp: NextPage<SignUpPageProps> = () => {
  const loginControllerApi = useSelector<RootState, LoginControllerApi>(
    ({ api }) => api.loginControllerApi
  );

  const dispatch = useDispatch();

  const handleSubmit = useCallback<SignUpPageLayoutProps['onSubmit']>(
    async values => {
      try {
        const { token } = await loginControllerApi.apiUsersSignupPost(values);
        signIn({ token });
      } catch (error) {
        dispatch(setError(error));
      }
    },
    [dispatch, loginControllerApi]
  );

  return <SignUpPageLayout onSubmit={handleSubmit} />;
};

export default SignUp;
