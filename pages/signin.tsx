import { NextPage } from 'next';
import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  SignInPageLayout,
  SignInPageLayoutProps
} from '../src/layouts/SignInPageLayout';
import { setError } from '../src/redux/error/actions';
import { RootState } from '../src/redux/store';
import { LoginControllerApi } from '../src/swagger/api';
import { signIn } from '../src/utils/auth';

export const config = { amp: 'hybrid' };

const SignIn: NextPage = () => {
  const loginControllerApi = useSelector<RootState, LoginControllerApi>(
    ({ api }) => api.loginControllerApi
  );

  const dispatch = useDispatch();

  const handleSubmit = useCallback<SignInPageLayoutProps['onSubmit']>(
    async values => {
      try {
        const { token } = await loginControllerApi.apiUsersSigninPost(values);
        signIn({ token });
      } catch (error) {
        dispatch(setError(error));
      }
    },
    [dispatch, loginControllerApi]
  );

  return <SignInPageLayout onSubmit={handleSubmit} />;
};

export default SignIn;
