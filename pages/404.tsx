import React from 'react';
import { NotFoundLayout } from '../src/layouts/NotFoundLayout';

const Custom404 = () => {
  return <NotFoundLayout />;
};

export default Custom404;
