import { NextPage } from 'next';
import React from 'react';
import { MainPageLayout } from '../src/layouts/MainPageLayout';
import { withAuthSync } from '../src/utils/auth';

export const config = { amp: 'hybrid' };

const Home: NextPage = () => {
  return <MainPageLayout />;
};

export default withAuthSync(false)(Home);
