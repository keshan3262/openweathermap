import { NextPage } from 'next';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import io, { Socket } from 'socket.io-client';
import { ForecastPageLayout, FormValues } from '../src/layouts/forecast-page';
import {
  getForecast,
  useHandleWeatherForecastReceiveCallback
} from '../src/redux/forecast/actions';
import { withAuthSync } from '../src/utils/auth';
import { UserData } from '../src/redux/auth/types';
import { setError } from '../src/redux/error/actions';
import { RootState } from '../src/redux/store';

export const config = { amp: 'hybrid' };

const ForecastPage: NextPage = () => {
  const [socket, setSocket] = useState<typeof Socket | undefined>();
  const [subscribed, setSubscribed] = useState(false);

  const userData = useSelector<RootState, UserData | undefined>(
    state => state.auth.userData
  );

  const dispatch = useDispatch();
  const handleWeatherForecastReceive = useHandleWeatherForecastReceiveCallback();

  useEffect(() => {
    const newSocket = io({ port: '3001' });

    setSocket(newSocket);

    return () => {
      newSocket.close();
    };
  }, []);

  useEffect(() => {
    if (socket) {
      console.log('adding weatherForecast event handler');
      socket.on('weatherForecast', handleWeatherForecastReceive);
    }
  }, [socket, handleWeatherForecastReceive]);

  useEffect(() => {
    if (socket) {
      console.log('adding error event handler');

      socket.on('epicFail', (message: string) => {
        dispatch(setError(new Error(message)));
      });
    }
  }, [dispatch, socket]);

  const handleSubmit = useCallback(
    (values: FormValues) => {
      if (socket) {
        console.log('subscribing');

        socket.emit(
          'subscribeWeatherForecast',
          JSON.stringify({
            Authorization: userData?.token,
            cityId: values.city,
            units: values.units
          })
        );

        setSubscribed(true);

        return;
      }

      dispatch(
        getForecast({
          location: values.city,
          units: values.units
        })
      );
    },
    [dispatch, socket, userData]
  );

  const handleChange = useCallback(() => {
    if (socket && subscribed) {
      console.log('unsubscribing');
      setSubscribed(false);
      socket.emit('unsubscribeWeatherForecast');
    }
  }, [socket, subscribed]);

  return <ForecastPageLayout onChange={handleChange} onSubmit={handleSubmit} />;
};

export default withAuthSync(true)(ForecastPage);
