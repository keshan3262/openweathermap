import apiRoutes from './server/routes';
import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import http from 'http';
import mongoose from 'mongoose';
import next from 'next';
import fetch from 'node-fetch';
import makeIo from 'socket.io';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import url from 'url';
import { validateAuthHeader } from './server/middleware/auth';

dotenv.config();
const dev = process.env.NODE_ENV !== 'production';
const test = process.env.NODE_ENV === 'test';
const app = next({ dev });
const handle = app.getRequestHandler();

const swaggerDefinition = {
  basePath: '/',
  host: `localhost:3000`,
  info: {
    description: 'API for custom openweathermap service',
    title: 'openweathermap',
    version: '0.0.0'
  }
};

const swaggerSpec = swaggerJSDoc({
  apis: [
    './server/middleware/*.ts',
    './server/models/*.ts',
    './server/models/*.yaml',
    './server/routes/*.ts',
    './server/routes/tags.yaml'
  ],
  swaggerDefinition
});

app.prepare().then(() => {
  const server = express();

  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({ extended: true }));

  /* eslint-disable immutable/no-mutation */
  mongoose.Promise = global.Promise;

  mongoose.connect(`mongodb://localhost/openweathermap${test ? '_test' : ''}`, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  /* eslint-enable immutable/no-mutation */

  if (dev) {
    server.use(
      cors({
        origin: /^http:\/\/localhost(:[0-9]+)?/
      })
    );
  }

  server.use(apiRoutes);

  server.use(
    '/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerSpec, { explorer: true })
  );

  server.get('/api-docs.json', (req, res) => res.json(swaggerSpec));

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  const socketServer = http.createServer(server);
  const io = makeIo(socketServer);

  io.on('connection', socket => {
    let weatherForecastIntervalDescriptor: NodeJS.Timeout | undefined;

    const unsubscribeWeatherForecast = () => {
      if (weatherForecastIntervalDescriptor) {
        clearInterval(weatherForecastIntervalDescriptor);
        weatherForecastIntervalDescriptor = undefined;
      }
    };

    const getWeatherForecast = async (message: string) => {
      const params = JSON.parse(message);

      const { Authorization, ...restParams } = params;

      const response = await fetch(
        url.format({
          pathname: 'http://localhost:3000/api/forecast',
          query: restParams
        }),
        { headers: { Authorization } }
      );

      return JSON.stringify(await response.json());
    };

    socket.on('subscribeWeatherForecast', async (message: string) => {
      try {
        const { Authorization } = JSON.parse(message);

        if (!Authorization) {
          throw new Error('Authorization required');
        }

        await validateAuthHeader(Authorization);

        const response = await getWeatherForecast(message);
        socket.emit('weatherForecast', response);

        unsubscribeWeatherForecast();

        weatherForecastIntervalDescriptor = setInterval(async () => {
          const response2 = await getWeatherForecast(message);
          socket.emit('weatherForecast', response2);
        }, 1000 * 60 * 30);
      } catch (error) {
        socket.emit('epicFail', error.message);
      }
    });

    socket.on('unsubscribeWeatherForecast', () => unsubscribeWeatherForecast);

    socket.on('disconnect', () => {
      unsubscribeWeatherForecast();
    });
  });

  socketServer.listen(3000, () => {
    console.log('listening on *:3000');
  });
});
