const mongoose = require('mongoose');

const test = process.env.NODE_ENV === 'test';

console.log('Parsing input file...');
const citiesData = require('./city.list.json');

mongoose.Promise = global.Promise;
console.log('Connecting to database...');
mongoose.connect(`mongodb://localhost/openweathermap${test ? '_test' : ''}`, {
  useCreateIndex: true,
  useFindAndModify: false,
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(async() => {
  const CitySchema = mongoose.Schema({
    country: {
      index: true,
      required: false,
      type: String
    },
    id: {
      required: true,
      type: Number,
      unique: true
    },
    location: {
      type: {
        enum: ['Point'],
        required: true,
        type: String
      },
      coordinates: {
        type: [Number],
        required: true
      }
    },
    name: {
      required: true,
      text: true,
      type: String
    },
    state: {
      required: false,
      type: String
    }
  });
  CitySchema.index({ name: 1 });

  const CityModel = mongoose.model('City', CitySchema);

  console.log('Removing already existing data...');
  await CityModel.deleteMany({});

  console.log('Inserting data...');
  await CityModel.insertMany(
    citiesData.map(({ coord, ...restProps }) => ({
      ...restProps,
      location: {
        coordinates: [coord.lon, coord.lat],
        type: 'Point'
      }
    }))
  );

  console.log('Successful');
  process.exit(0);
}).then().catch(
  err => {
    console.error(err);
    process.exit(1);
  }
);