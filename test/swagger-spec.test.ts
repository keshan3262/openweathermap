import assert from 'assert';
import request from 'supertest';

const testsFn = () => {
  it('should provide GUI version', done => {
    request('http://localhost:3000')
      .get('/api-docs')
      .expect(res => {
        assert.ok(res.status < 400);

        assert.equal(
          res.header['content-type']?.toLowerCase(),
          'text/html; charset=utf-8'
        );

        done();
      })
      .catch(error => {
        console.error(error);
        done(error);
      });
  });

  it('should provide JSON version', done => {
    request('http://localhost:3000')
      .get('/api-docs.json')
      .expect(res => {
        assert.ok(res.status < 400);

        assert.equal(
          res.header['content-type']?.toLowerCase(),
          'application/json; charset=utf-8'
        );

        assert.equal(typeof res.body, 'object');
        assert.equal(typeof res.body.swagger, 'string');
        done();
      })
      .catch(error => {
        console.error(error);
        done(error);
      });
  });
};

export default testsFn;
