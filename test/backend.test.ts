import childProcess from 'child_process';
import { noop } from 'lodash';
import { after, before } from 'mocha';
import mongoose from 'mongoose';
import request from 'supertest';
import swaggerTestsFn from './swagger-spec.test';
import citiesApiTestFn from './api/cities.test';
import { mockCities } from './test-data';
import { City as CityModel } from '../server/models/city';
import { User } from '../server/models/user';

let p1: childProcess.ChildProcess;

describe('backend', () => {
  before(done => {
    p1 = childProcess.spawn(
      './node_modules/.bin/ts-node',
      ['-P', 'tsconfig.backend.json', 'server.ts'],
      { stdio: 'inherit' }
    );

    console.log('Waiting for server to start...');

    const checkInterval = 1000;
    const timeout = 60000;
    const startTimestamp = Date.now();

    const intervalDescriptor = setInterval(() => {
      if (Date.now() >= startTimestamp + timeout) {
        clearInterval(intervalDescriptor);

        throw new Error('Timeout exceeded');
      }

      request('http://localhost:3000')
        .get('/')
        .timeout(checkInterval - 1)
        .then(() => {
          console.log('Started successfully');
          clearInterval(intervalDescriptor);
          done();
        })
        .catch(noop);
    }, checkInterval);
  });

  describe('swagger specification', swaggerTestsFn);

  describe('API entries', () => {
    const email = 'test.user@mail.com';
    const nickname = 'Test User';
    const password = 'weakpassword';

    describe('Entries which require authorization', () => {
      before(async () => {
        console.log('Connecting to database...');

        await mongoose.connect(`mongodb://localhost/openweathermap_test`, {
          useCreateIndex: true,
          useFindAndModify: false,
          useNewUrlParser: true,
          useUnifiedTopology: true
        });

        console.log('Importing cities...');
        await CityModel.insertMany(mockCities);

        console.log('Creating a test user...');

        await request('http://localhost:3000')
          .post('/api/users/signup')
          .send({
            email,
            nickname,
            password
          });
      });

      describe('/api/cities', () => citiesApiTestFn(email, password));
    });
  });

  after(async () => {
    console.log('Stopping server...');
    p1.kill();

    console.log('Deleting test data...');
    await CityModel.deleteMany({});
    await User.deleteMany({});
    await mongoose.disconnect();
  });
});
