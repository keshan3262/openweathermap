import assert from 'assert';
import { before } from 'mocha';
import request from 'supertest';
import { City } from '../../src/swagger/api';

const testsFn = (email: string, password: string) => {
  describe('/api/cities', () => {
    let authHeader: string;

    before(async () => {
      authHeader = (
        await request('http://localhost:3000')
          .post('/api/users/signin')
          .send({ email, password })
      ).header['authorization'];
    });

    it("should give an error response with status 401 if a user isn't authorized", done => {
      request('http://localhost:3000')
        .get('/api/cities')
        .query({ country: 'US', name: 'Odessa' })
        .expect(res => {
          assert.equal(res.status, 401);
          assert.equal(res.body, 'Authorization required');
          done();
        })
        .catch(error => {
          console.error(error);
          done(error);
        });
    });

    it('should provide list of cities which names start with a string or contains given words', async () => {
      const cityNameWord = 'Odessa';
      const cityNameFragment = 'Ode';

      const cityNameStartsWith = (str: string) => (city: City) =>
        city.name.startsWith(str);

      const cityNameIncludes = (str: string) => (city: City) =>
        city.name.includes(str);

      // USA has some cities with names which start with 'Odessa' or contain this word
      console.log(authHeader);

      await request('http://localhost:3000')
        .get('/api/cities')
        .query({ country: 'US', name: cityNameFragment })
        .set('Authorization', authHeader)
        .expect(res => {
          const cityMatchesSearch = (city: City) =>
            cityNameStartsWith(cityNameFragment)(city) ||
            cityNameIncludes(cityNameFragment)(city);

          assert.ok(res.status < 400);
          assert.ok(res.body instanceof Array);
          assert.ok(res.body.length > 0);
          assert.ok(res.body.every(cityMatchesSearch));
        });

      await request('http://localhost:3000')
        .get('/api/cities')
        .query({ country: 'US', name: cityNameWord })
        .set('Authorization', authHeader)
        .expect(res => {
          const cityMatchesSearch = (city: City) =>
            cityNameStartsWith(cityNameWord)(city) ||
            cityNameIncludes(cityNameWord)(city);

          assert.ok(res.status < 400);
          assert.ok(res.body instanceof Array);
          assert.ok(res.body.length > 0);
          assert.ok(res.body.every(cityMatchesSearch));

          const citiesContainingWord = res.body.filter((city: City) =>
            city.name.includes(cityNameWord)
          );

          const citiesStartingWithWord = res.body.filter((city: City) =>
            city.name.startsWith(cityNameWord)
          );

          assert.ok(
            citiesContainingWord.length > citiesStartingWithWord.length
          );

          assert.ok(citiesStartingWithWord.length > 0);
        });
    });
  });
};

export default testsFn;
