import { Document } from 'mongoose';
import { CityDocument } from '../server/models/city';

type RawCity = Omit<CityDocument, keyof Document> & { id: number };

export const mockCities: RawCity[] = [
  {
    country: 'UA',
    id: 698740,
    location: {
      coordinates: [30.732622, 46.477474],
      type: 'Point'
    },
    name: 'Odessa',
    state: ''
  },
  {
    country: 'UA',
    id: 808832,
    location: {
      coordinates: [36.323059, 49.061668],
      type: 'Point'
    },
    name: 'Oddykhne',
    state: ''
  },
  {
    country: 'UA',
    id: 698738,
    location: {
      coordinates: [30, 47],
      type: 'Point'
    },
    name: "Odes'ka Oblast'",
    state: ''
  },
  {
    location: { coordinates: [-95.250549, 42.31221], type: 'Point' },
    id: 4869743,
    name: 'Odebolt',
    state: 'IA',
    country: 'US'
  },
  {
    location: { coordinates: [-88.525337, 41.003639], type: 'Point' },
    id: 4904574,
    name: 'Odell',
    state: 'IL',
    country: 'US'
  },
  {
    location: { coordinates: [-121.543129, 45.62706], type: 'Point' },
    id: 5743896,
    name: 'Odell',
    state: 'OR',
    country: 'US'
  },
  {
    location: { coordinates: [-97.582222, 27.950569], type: 'Point' },
    id: 4716128,
    name: 'Odem',
    state: 'TX',
    country: 'US'
  },
  {
    location: { coordinates: [-76.700249, 39.084], type: 'Point' },
    id: 4364362,
    name: 'Odenton',
    state: 'MD',
    country: 'US'
  },
  {
    location: { coordinates: [-86.396652, 33.677319], type: 'Point' },
    id: 4081166,
    name: 'Odenville',
    state: 'AL',
    country: 'US'
  },
  {
    location: { coordinates: [-82.591759, 28.193899], type: 'Point' },
    id: 4166787,
    name: 'Odessa',
    state: 'FL',
    country: 'US'
  },
  {
    location: { coordinates: [-93.95356, 38.999168], type: 'Point' },
    id: 4401659,
    name: 'Odessa',
    state: 'MO',
    country: 'US'
  },
  {
    location: { coordinates: [-102.367638, 31.84568], type: 'Point' },
    id: 5527554,
    name: 'Odessa',
    state: 'TX',
    country: 'US'
  },
  {
    location: { coordinates: [-119.645958, 36.271339], type: 'Point' },
    id: 5378819,
    name: 'Odessa (historical)',
    state: 'CA',
    country: 'US'
  },
  {
    location: { coordinates: [-89.052292, 38.617271], type: 'Point' },
    id: 4246147,
    name: 'Odin',
    state: 'IL',
    country: 'US'
  },
  {
    location: { coordinates: [-86.991402, 38.842831], type: 'Point' },
    id: 4262451,
    name: 'Odon',
    state: 'IN',
    country: 'US'
  },
  {
    location: { coordinates: [-85.138344, 42.78476], type: 'Point' },
    id: 4998583,
    name: 'Lake Odessa',
    state: 'MI',
    country: 'US'
  },
  {
    location: { coordinates: [-102.498756, 31.84235], type: 'Point' },
    id: 5533366,
    name: 'West Odessa',
    state: 'TX',
    country: 'US'
  }
];
