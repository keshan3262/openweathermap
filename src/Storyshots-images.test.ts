import initStoryshots from '@storybook/addon-storyshots';
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';
import { MatchImageSnapshotOptions } from 'jest-image-snapshot';
import * as process from 'process';
import { DirectNavigationOptions } from 'puppeteer';

const getMatchOptions = (): MatchImageSnapshotOptions => ({
  customDiffConfig: {
    threshold: 0.001
  },
  failureThreshold: 0.001,
  failureThresholdType: 'percent'
});

const getGotoOptions = (): DirectNavigationOptions => {
  return {
    waitUntil: 'networkidle0'
  };
};

const storybookUrl = process.env.STORYBOOK_URL || 'http://localhost:6006/';

initStoryshots({
  suite: 'Image storyshots',
  test: imageSnapshot({
    customizePage: page => page.setViewport({ height: 900, width: 1440 }),
    getGotoOptions,
    getMatchOptions,
    storybookUrl
  })
});
