import { without } from 'lodash';
import { useCallback, useState } from 'react';

type KeyPrototype = string | number | undefined;

function useExpansionsState<Key extends KeyPrototype>() {
  const [expandedItemsKeys, setExpandedItemsKeys] = useState<Key[]>([]);

  const handleExpansionChange = useCallback(
    (key: Key, isExpanded: boolean) => {
      if (isExpanded) {
        setExpandedItemsKeys([...expandedItemsKeys, key]);
      } else {
        setExpandedItemsKeys(without(expandedItemsKeys, key));
      }
    },
    [expandedItemsKeys, setExpandedItemsKeys]
  );

  return {
    expandedItemsKeys,
    handleExpansionChange,
    setExpandedItemsKeys
  };
}

export default useExpansionsState;
