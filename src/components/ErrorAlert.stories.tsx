import { storiesOf } from '@storybook/react';
import { Response } from 'node-fetch';
import React from 'react';
import withRedux from '../utils/with-redux';
import { ErrorAlert } from './ErrorAlert';

const makeFetchError = (message: string, code?: string) => {
  return {
    ...new Error(),
    code,
    message,
    name: 'FetchError',
    type: 'mock_error'
  };
};

storiesOf('components/ErrorAlert', module)
  .add('no error in redux state', () => withRedux()(() => <ErrorAlert />))
  .add('with Response that indicates error', () =>
    withRedux({
      error: {
        error: new Response('"Authorization required"', { status: 401 })
      }
    })(() => <ErrorAlert />)
  )
  .add('with FetchError that has code 404', () =>
    withRedux({
      error: {
        error: makeFetchError('Error message', '404')
      }
    })(() => <ErrorAlert />)
  )
  .add('with FetchError that has no code', () =>
    withRedux({
      error: {
        error: makeFetchError('Error message')
      }
    })(() => <ErrorAlert />)
  )
  .add('with other error', () =>
    withRedux({
      error: {
        error: new Error('Some error')
      }
    })(() => <ErrorAlert />)
  );
