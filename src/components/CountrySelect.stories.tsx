import { storiesOf } from '@storybook/react';
import React from 'react';
import { CountrySelect } from './CountrySelect';
import makeReduxFormDecorator from '../utils/redux-form-decorator';

const noValueStoryDecorator = makeReduxFormDecorator({ form: 'mockForm' });

storiesOf('components/CountrySelect[NO-JEST-STORYSHOT]', module)
  .add('default state', () =>
    noValueStoryDecorator(() => <CountrySelect name="inputName" />)
  )
  .add('compact by width', () =>
    noValueStoryDecorator(() => (
      <CountrySelect name="inputName" fullWidth={false} />
    ))
  )
  .add('with a helper text', () =>
    noValueStoryDecorator(() => (
      <CountrySelect name="inputName" helperText="This is a helper text" />
    ))
  )
  .add('with dense margin', () =>
    noValueStoryDecorator(() => (
      <CountrySelect name="inputName" margin="dense" />
    ))
  )
  .add('standard Material UI variant', () =>
    noValueStoryDecorator(() => (
      <CountrySelect name="inputName" variant="standard" />
    ))
  )
  .add('open', () =>
    noValueStoryDecorator(() => <CountrySelect name="inputName" open={true} />)
  )
  .add('with already selected country', () =>
    makeReduxFormDecorator({
      form: 'mockForm',
      initialValues: { inputName: 'US' }
    })(() => <CountrySelect name="inputName" />)
  );
