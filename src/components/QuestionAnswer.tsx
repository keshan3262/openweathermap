import { Typography } from '@material-ui/core';
import React from 'react';

export type QuestionAnswerProps = {
  children: React.ReactNode;
  question: string;
};

export const QuestionAnswer = React.memo(function QuestionAnswer(
  props: QuestionAnswerProps
) {
  const { children, question } = props;

  return (
    <React.Fragment>
      <Typography component="h1" gutterBottom={true} variant="h4">
        {question}
      </Typography>

      <Typography variant="body1">{children}</Typography>
    </React.Fragment>
  );
});
