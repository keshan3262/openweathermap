import { storiesOf } from '@storybook/react';
import React from 'react';
import { ConnectedTextField } from './ConnectedTextField';
import makeReduxFormDecorator from '../utils/redux-form-decorator';

const noValueStoryDecorator = makeReduxFormDecorator({ form: 'mockForm' });

storiesOf('components/ConnectedTextField', module)
  .add('default state', () =>
    noValueStoryDecorator(() => <ConnectedTextField name="inputName" />)
  )
  .add('compact by width', () =>
    noValueStoryDecorator(() => (
      <ConnectedTextField fullWidth={false} name="inputName" />
    ))
  )
  .add('with dense margin', () =>
    noValueStoryDecorator(() => (
      <ConnectedTextField margin="dense" name="inputName" />
    ))
  )
  .add('standard Material UI variant', () =>
    noValueStoryDecorator(() => (
      <ConnectedTextField name="inputName" variant="standard" />
    ))
  )
  .add('filled', () =>
    makeReduxFormDecorator({
      form: 'mockForm',
      initialValues: { inputName: 'Input value' }
    })(() => <ConnectedTextField name="inputName" />)
  );
