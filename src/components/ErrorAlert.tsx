import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle
} from '@material-ui/core';
import { FetchError, Response } from 'node-fetch';
import React, { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../redux/store';

const buildAlertContent = async (error: Error | Response) => {
  if (error instanceof Response) {
    return JSON.stringify(await error.json());
  }

  if (error.name === 'FetchError') {
    const { code, message } = error as FetchError;

    return `Request failed${code ? ` with code ${code}` : ''}: ${message}`;
  }

  return error.message || error.name;
};

export const ErrorAlert = () => {
  const error = useSelector<RootState, Error | Response | undefined>(
    state => state.error.error
  );

  const [alertContent, setAlertContent] = useState<string>();

  useEffect(() => {
    if (error) {
      buildAlertContent(error).then(content => setAlertContent(content));
    } else {
      setAlertContent(undefined);
    }
  }, [error, setAlertContent]);

  const handleClose = useCallback(() => setAlertContent(undefined), [
    setAlertContent
  ]);

  return (
    <Dialog
      aria-describedby="error-alert-description"
      aria-labelledby="error-alert-title"
      onClose={handleClose}
      open={!!alertContent}
    >
      <DialogTitle id="error-alert-title">Error</DialogTitle>

      <DialogContent>
        <DialogContentText id="error-alert-description">
          {alertContent}
        </DialogContentText>
      </DialogContent>
    </Dialog>
  );
};
