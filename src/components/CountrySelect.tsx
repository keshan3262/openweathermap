import { TextField, TextFieldProps } from '@material-ui/core';
import {
  Autocomplete,
  AutocompleteProps,
  RenderInputParams
} from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import React, { useCallback, useMemo } from 'react';
import { Field, WrappedFieldProps } from 'redux-form';
import { countries, CountryType } from '../constants/countries';

export type CountrySelectProps = Pick<
  TextFieldProps,
  'fullWidth' | 'helperText' | 'margin' | 'variant'
> & {
  name: string;
  open?: boolean;
};

type CountryOptionProps = CountryType & {
  classes: Record<'flag', string>;
};

const useStyles = makeStyles(theme => ({
  flag: {
    marginRight: theme.spacing(0.5)
  }
}));

// ISO 3166-1 alpha-2
// ⚠️ No support for IE 11
const countryToFlag = (isoCode: string) => {
  return typeof String.fromCodePoint !== 'undefined'
    ? isoCode
        .toUpperCase()
        .replace(/./g, char =>
          String.fromCodePoint(char.charCodeAt(0) + 127397)
        )
    : isoCode;
};

const getOptionLabel = ({ code, label }: CountryType) => {
  if (code === '-') {
    return '-';
  }

  return `${label} (${code})`;
};

const CountryOption = (props: CountryOptionProps) => {
  const { classes, code, label } = props;

  if (code === '-') {
    return <span>-</span>;
  }

  return (
    <React.Fragment>
      <span className={classes.flag}>{countryToFlag(code)}</span>
      {getOptionLabel({ code, label })}
    </React.Fragment>
  );
};

const CountryAutocompletion = (
  props: WrappedFieldProps & {
    open?: boolean;
    renderInput: AutocompleteProps<CountryType>['renderInput'];
  }
) => {
  const {
    input: { onChange, onFocus, value },
    open,
    renderInput
  } = props;

  const classes = useStyles();

  const renderOption = useCallback(
    (option: CountryType) => <CountryOption {...option} classes={classes} />,
    [classes]
  );

  const autocompleteValue = useMemo(() => {
    return countries.find(({ code }) => code === value);
  }, [value]);

  const handleChange = useCallback(
    (event: React.ChangeEvent<{}>, newValue: CountryType | null) => {
      onChange(newValue?.code);
    },
    [onChange]
  );

  return (
    <Autocomplete
      options={countries}
      autoHighlight={true}
      getOptionLabel={getOptionLabel}
      onChange={handleChange}
      onFocus={onFocus}
      open={open}
      renderOption={renderOption}
      renderInput={renderInput}
      value={autocompleteValue}
    />
  );
};

export const CountrySelect = (props: CountrySelectProps) => {
  const {
    fullWidth = true,
    helperText,
    margin = 'normal',
    name,
    open,
    variant = 'outlined'
  } = props;

  const renderInput = useCallback<React.FunctionComponent<RenderInputParams>>(
    params => (
      <TextField
        {...params}
        fullWidth={fullWidth}
        helperText={helperText}
        margin={margin}
        variant={variant}
      />
    ),
    [fullWidth, helperText, margin, variant]
  );

  const renderAutocompletion = useCallback<
    React.FunctionComponent<WrappedFieldProps & { open?: boolean }>
  >(params => <CountryAutocompletion {...params} renderInput={renderInput} />, [
    renderInput
  ]);

  return <Field component={renderAutocompletion} name={name} open={open} />;
};
