import { storiesOf } from '@storybook/react';
import React from 'react';
import { QuestionAnswer } from './QuestionAnswer';

storiesOf('components/QuestionAnswer', module).add('default usecase', () => (
  <QuestionAnswer question="Why should I use storybook?">
    It makes catching components design regression easier.
  </QuestionAnswer>
));
