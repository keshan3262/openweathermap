import {
  TextField as MaterialTextField,
  TextFieldProps as MaterialTextFieldProps
} from '@material-ui/core';
import React, { useCallback } from 'react';
import { Field, WrappedFieldProps } from 'redux-form';

export type ConnectedTextFieldProps = Omit<
  MaterialTextFieldProps,
  'defaultValue' | 'error' | 'onChange' | 'onFocus' | 'ref' | 'value'
> & {
  innerRef?: MaterialTextFieldProps['ref'];
};

export const ConnectedTextField = (props: ConnectedTextFieldProps) => {
  const { fullWidth = true, margin = 'normal', size, ...restProps } = props;

  const renderInput = useCallback(
    ({
      innerRef,
      input,
      variant,
      ...restInputProps
    }: WrappedFieldProps & ConnectedTextFieldProps) => (
      <MaterialTextField
        {...input}
        {...restInputProps}
        ref={innerRef}
        size={size}
        variant={variant || 'outlined'}
      />
    ),
    [size]
  );

  return (
    <Field
      {...restProps}
      component={renderInput}
      fullWidth={fullWidth}
      margin={margin}
    />
  );
};
