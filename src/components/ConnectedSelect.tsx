import {
  FormControl,
  Select as MaterialSelect,
  SelectProps as MaterialSelectProps
} from '@material-ui/core';
import React from 'react';
import { Field, WrappedFieldProps } from 'redux-form';

export type ConnectedSelectProps = Omit<
  MaterialSelectProps,
  'defaultValue' | 'error' | 'onChange' | 'onFocus' | 'ref' | 'value'
> & {
  innerRef?: MaterialSelectProps['ref'];
  showError?: boolean;
  showSeparateLabel?: boolean;
};

const renderSelectField = (props: WrappedFieldProps & ConnectedSelectProps) => {
  const {
    fullWidth,
    innerRef,
    input: { key, ...restInputProps },
    label,
    meta: { error, visited },
    name,
    showError,
    showSeparateLabel,
    ...restProps
  } = props;

  return (
    <FormControl
      error={showError && visited ? error : undefined}
      fullWidth={fullWidth}
    >
      {showSeparateLabel && <label htmlFor={name}>{label}</label>}

      <MaterialSelect
        {...restInputProps}
        {...restProps}
        fullWidth={fullWidth}
        key={key || undefined}
        label={showSeparateLabel ? undefined : label}
        name={name}
        ref={innerRef}
      />
    </FormControl>
  );
};

export const ConnectedSelect = (props: ConnectedSelectProps) => {
  const { fullWidth = true, variant = 'outlined', ...restProps } = props;

  return (
    <Field
      {...restProps}
      component={renderSelectField}
      fullWidth={fullWidth}
      variant={variant}
    />
  );
};
