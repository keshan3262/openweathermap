import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { Navbar, NavbarProps } from './Navbar';

const useStyles = makeStyles(theme => ({
  main: {
    marginBottom: theme.spacing(8),
    marginTop: theme.spacing(2)
  }
}));

export interface PageWrapperProps extends NavbarProps {
  children: React.ReactNode;
}

export const PageWrapper = React.memo(function PageWrapper(
  props: PageWrapperProps
) {
  const { children, ...navbarProps } = props;

  const classes = useStyles();

  return (
    <div>
      <Navbar {...navbarProps} />

      <Container
        className={classes.main}
        component="main"
        disableGutters={true}
        maxWidth={false}
      >
        {children}
      </Container>
    </div>
  );
});
