import { storiesOf } from '@storybook/react';
import React from 'react';
import { PageWrapper } from './PageWrapper';
import withRedux from '../utils/with-redux';
import { forecastPath } from '../constants/routes';

const loggedInStoryDecorator = withRedux({
  auth: {
    userData: {
      email: 'fake_email@gmail.com',
      nickname: 'Storybook user',
      token: 'fakeToken'
    }
  }
});

storiesOf('components/PageWrapper[NO-JEST-STORYSHOT]', module)
  .add('main page, not logged in', () =>
    withRedux()(() => <PageWrapper route="/">children</PageWrapper>)
  )
  .add('main page, logged in', () =>
    loggedInStoryDecorator(() => (
      <PageWrapper route="/">Stub content for main page</PageWrapper>
    ))
  )
  .add('weather forecast page, logged in', () =>
    loggedInStoryDecorator(() => (
      <PageWrapper route={forecastPath}>
        Stub content for weather forecast page
      </PageWrapper>
    ))
  );
