import { debounce } from 'lodash';
import React, { useCallback, useMemo } from 'react';
import { CircularProgress, TextField, TextFieldProps } from '@material-ui/core';
import Autocomplete, {
  AutocompleteProps,
  RenderInputParams
} from '@material-ui/lab/Autocomplete';
import { useSelector } from 'react-redux';
import { Field, WrappedFieldProps } from 'redux-form';
import { RootState } from '../redux/store';
import { City } from '../swagger/api';

type AutocompleteInputChangeCallback = NonNullable<
  AutocompleteProps<City>['onInputChange']
>;
type AutocompleteChangeCallback = (
  event: React.ChangeEvent<{}>,
  value: City | null
) => void;
type AdvancedCityAutocompletionProps = Pick<
  AutocompleteProps<City>,
  'disabled' | 'inputValue' | 'loading' | 'options' | 'renderInput'
> & {
  onInputChange: (input: string) => void;
};

export type CitySelectProps = Pick<
  TextFieldProps,
  'disabled' | 'margin' | 'variant'
> &
  Pick<
    AdvancedCityAutocompletionProps,
    'loading' | 'inputValue' | 'onInputChange' | 'options'
  > & {
    name: string;
  };

const CityTextInput = (
  props: RenderInputParams & Pick<CitySelectProps, 'margin' | 'variant'>
) => {
  const {
    InputProps,
    margin = 'normal',
    variant = 'outlined',
    ...restProps
  } = props;

  const loading = useSelector<RootState, boolean>(
    state => state.cities.loading
  );

  return (
    <TextField
      {...restProps}
      InputProps={{
        ...InputProps,
        endAdornment: (
          <React.Fragment>
            {loading ? <CircularProgress color="inherit" size={20} /> : null}
            {InputProps.endAdornment}
          </React.Fragment>
        )
      }}
      margin={margin}
      variant={variant}
    />
  );
};

const getOptionLabel = ({ location, name, state }: City) => {
  return `${name} (${state ? `${state}, ` : ''}${location.lat} ${
    location.lng
  })`;
};

const CityAutocompletion = (
  props: WrappedFieldProps & AdvancedCityAutocompletionProps
) => {
  const {
    disabled,
    input: { onChange, onFocus, value },
    inputValue,
    loading,
    onInputChange,
    options,
    renderInput
  } = props;

  const handleChange = useCallback<AutocompleteChangeCallback>(
    (event, newValue) => {
      onChange(newValue?.id);
    },
    [onChange]
  );

  const handleInputChange = useCallback<AutocompleteInputChangeCallback>(
    (event, newValue) => {
      onInputChange(newValue);
    },
    [onInputChange]
  );

  const getOptionSelected = useCallback((city: City) => city.id === value, [
    value
  ]);

  return (
    <Autocomplete<City>
      disabled={disabled}
      getOptionLabel={getOptionLabel}
      getOptionSelected={getOptionSelected}
      inputValue={inputValue}
      onChange={handleChange}
      onFocus={onFocus}
      onInputChange={handleInputChange}
      options={options}
      loading={loading}
      renderInput={renderInput}
    />
  );
};

export const CitySelect = (props: CitySelectProps) => {
  const {
    disabled,
    inputValue,
    loading,
    margin,
    name,
    onInputChange,
    options,
    variant
  } = props;

  const handleInputChange = useMemo(
    () =>
      debounce<AdvancedCityAutocompletionProps['onInputChange']>(
        newInputValue => {
          onInputChange(newInputValue);
        },
        200
      ),
    [onInputChange]
  );

  const renderTextField = useCallback<AutocompleteProps<City>['renderInput']>(
    params => <CityTextInput {...params} margin={margin} variant={variant} />,
    [margin, variant]
  );

  return (
    <Field
      component={CityAutocompletion}
      disabled={disabled}
      inputValue={inputValue}
      loading={loading}
      name={name}
      onInputChange={handleInputChange}
      options={options}
      renderInput={renderTextField}
    />
  );
};
