import { AppBar, Button, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  forecastPath,
  paths,
  pathsWithoutNavbar,
  signInPath,
  signUpPath
} from '../constants/routes';
import Link from 'next/link';
import { findKey } from 'lodash';
import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../redux/store';
import { UserData } from '../redux/auth/types';
import { signOut } from '../utils/auth';

export interface NavbarProps {
  route: string;
}

const useStyles = makeStyles(theme => ({
  appBarSpacer: theme.mixins.toolbar,
  title: {
    flexGrow: 1
  }
}));

export const Navbar = React.memo(function Navbar(props: NavbarProps) {
  const { route } = props;

  const classes = useStyles();

  const matchingPath = useMemo(
    () => findKey(paths, ({ regex }) => regex.test(route)),
    [route]
  );

  const userData = useSelector<RootState, UserData | undefined>(
    state => state.auth.userData
  );

  const rightPart = useMemo(() => {
    if (userData) {
      return (
        <React.Fragment>
          <Typography variant="body1">Hi there, {userData.nickname}</Typography>

          <Link href={forecastPath}>
            <Button color="inherit" href={forecastPath}>
              {paths[forecastPath].title}
            </Button>
          </Link>
          <Button color="inherit" onClick={signOut}>
            Sign out
          </Button>
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        <Link href={signInPath}>
          <Button color="inherit" href={signInPath}>
            {paths[signInPath].title}
          </Button>
        </Link>

        <Link href={signUpPath}>
          <Button color="inherit" href={signUpPath}>
            {paths[signUpPath].title}
          </Button>
        </Link>
      </React.Fragment>
    );
  }, [userData]);

  if (matchingPath != null && pathsWithoutNavbar.includes(matchingPath)) {
    return null;
  }

  return (
    <React.Fragment>
      <AppBar position="fixed">
        <Toolbar>
          <Typography
            className={classes.title}
            color="inherit"
            component="h1"
            noWrap={true}
            variant="h6"
          >
            {matchingPath == null ? '' : paths[matchingPath].title}
          </Typography>

          {rightPart}
        </Toolbar>
      </AppBar>

      <div className={classes.appBarSpacer} />
    </React.Fragment>
  );
});
