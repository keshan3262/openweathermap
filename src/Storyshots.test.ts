import initStoryshots, {
  multiSnapshotWithOptions
} from '@storybook/addon-storyshots';

initStoryshots({
  integrityOptions: { cwd: __dirname }, // it will start searching from the current directory
  storyKindRegex: /^((?!.*?\[NO-JEST-STORYSHOT\]).)*$/,
  storyNameRegex: /^((?!.*?\[NO-JEST-STORYSHOT\]).)*$/,
  test: multiSnapshotWithOptions({})
});
