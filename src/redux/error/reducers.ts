import { ErrorAction, ErrorState, ERROR } from './types';
import { Reducer } from 'redux';

export const initialState: ErrorState = {};

const errorReducer: Reducer<ErrorState, ErrorAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case ERROR:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
};

export default errorReducer;
