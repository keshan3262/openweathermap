import { Response } from 'node-fetch';

export const ERROR = 'ERROR';

export type ErrorState = {
  error?: Error | Response;
};

export type SetErrorAction = {
  error?: Error | Response;
  type: typeof ERROR;
};

export type ErrorAction = SetErrorAction;
