import { Response } from 'node-fetch';
import { ERROR, SetErrorAction } from './types';

export const setError = (error?: Error | Response): SetErrorAction => ({
  error,
  type: ERROR
});
