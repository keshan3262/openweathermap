import {
  SIGNIN,
  SignInAction,
  SIGNOUT,
  SignOutAction,
  UserData
} from './types';

export const signIn = (value: UserData): SignInAction => ({
  type: SIGNIN,
  value
});

export const signOut = (): SignOutAction => ({
  type: SIGNOUT
});
