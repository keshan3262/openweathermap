export const SIGNIN = 'SIGNIN';
export const SIGNOUT = 'SIGNOUT';

export type UserData = {
  email: string;
  nickname: string;
  token: string;
};

export type AuthState = {
  userData?: UserData;
};

export type SignInAction = {
  type: typeof SIGNIN;
  value: UserData;
};

export type SignOutAction = {
  type: typeof SIGNOUT;
};

export type AuthAction = SignInAction | SignOutAction;
