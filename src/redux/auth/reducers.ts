import { AuthState, SIGNIN, SIGNOUT, AuthAction } from './types';
import { Reducer } from 'redux';

export const initialState: AuthState = {};

const authReducer: Reducer<AuthState, AuthAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case SIGNIN:
      return {
        ...state,
        userData: action.value
      };
    case SIGNOUT:
      return {
        ...state,
        userData: undefined
      };
    default:
      return state;
  }
};

export default authReducer;
