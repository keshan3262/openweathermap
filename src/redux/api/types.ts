import {
  CitiesControllerApi,
  ForecastControllerApi,
  LoginControllerApi
} from '../../swagger/api';

export type ApiState = {
  citiesControllerApi: CitiesControllerApi;
  forecastControllerApi: ForecastControllerApi;
  loginControllerApi: LoginControllerApi;
};
