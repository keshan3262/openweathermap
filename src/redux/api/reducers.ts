import {
  CitiesControllerApi,
  ForecastControllerApi,
  LoginControllerApi
} from '../../swagger/api';
import { ApiState } from './types';

export const initialState: ApiState = {
  citiesControllerApi: new CitiesControllerApi(),
  forecastControllerApi: new ForecastControllerApi(),
  loginControllerApi: new LoginControllerApi()
};

const apiReducer = (state = initialState) => state;

export default apiReducer;
