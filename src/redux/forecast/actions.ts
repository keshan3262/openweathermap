import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { ThunkAction } from 'redux-thunk';
import { Location, Units } from '../../swagger/api';
import {
  TransformedForecast,
  transformWeatherForecast
} from '../../utils/transform-weather-forecast';
import { setError } from '../error/actions';
import { ErrorAction } from '../error/types';
import { RootState } from '../store';
import {
  FORECAST_SET_LOADING,
  ForecastAction,
  RESET_FORECAST,
  ResetForecastAction,
  SetLoadingAction,
  UPDATE_FORECAST,
  UpdateForecastAction
} from './types';

export type GetForecastParams = {
  location: number | Location;
  units: Units;
};

export const setLoading = (value: boolean): SetLoadingAction => ({
  type: FORECAST_SET_LOADING,
  value
});

export const resetForecast = (): ResetForecastAction => ({
  type: RESET_FORECAST
});

export const updateForecast = (
  value: TransformedForecast
): UpdateForecastAction => ({
  type: UPDATE_FORECAST,
  value
});

export const getForecast = (
  params: GetForecastParams
): ThunkAction<void, RootState, [], ForecastAction | ErrorAction> => {
  const { location, units } = params;

  return async (dispatch, getState) => {
    const {
      api: { forecastControllerApi },
      auth: { userData }
    } = getState();

    try {
      if (!userData) {
        throw new Error('Authorization required');
      }

      dispatch(setLoading(true));

      const forecast = await forecastControllerApi.apiForecastGet(
        typeof location === 'number' ? location : undefined,
        undefined,
        typeof location === 'number' ? undefined : location.lat,
        typeof location === 'number' ? undefined : location.lng,
        units,
        { headers: { Authorization: userData.token } }
      );

      dispatch(updateForecast(transformWeatherForecast(forecast)));
    } catch (error) {
      dispatch(setError(error));
    } finally {
      dispatch(setLoading(false));
    }
  };
};

export const useHandleWeatherForecastReceiveCallback = () => {
  const dispatch = useDispatch();

  return useCallback(
    (newForecast: string) => {
      try {
        dispatch(
          updateForecast(transformWeatherForecast(JSON.parse(newForecast)))
        );
      } catch (error) {
        dispatch(setError(error));
      }
    },
    [dispatch]
  );
};
