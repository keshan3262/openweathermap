import { Reducer } from 'redux';
import {
  FORECAST_SET_LOADING,
  ForecastAction,
  ForecastState,
  RESET_FORECAST,
  UPDATE_FORECAST
} from './types';

export const initialState: ForecastState = {
  loading: false
};

const forecastReducer: Reducer<ForecastState, ForecastAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case UPDATE_FORECAST:
      return {
        ...state,
        forecast: action.value
      };
    case FORECAST_SET_LOADING:
      return {
        ...state,
        loading: action.value
      };
    case RESET_FORECAST:
      return {
        ...state,
        forecast: undefined
      };
    default:
      return state;
  }
};

export default forecastReducer;
