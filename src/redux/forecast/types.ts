import { TransformedForecast } from '../../utils/transform-weather-forecast';

export const UPDATE_FORECAST = 'UPDATE_FORECAST';
export const RESET_FORECAST = 'RESET_FORECAST';
export const FORECAST_SET_LOADING = 'SET_LOADING';

export type ForecastState = {
  forecast?: TransformedForecast;
  loading: boolean;
};

export type UpdateForecastAction = {
  type: typeof UPDATE_FORECAST;
  value: TransformedForecast;
};

export type ResetForecastAction = {
  type: typeof RESET_FORECAST;
};

export type SetLoadingAction = {
  type: typeof FORECAST_SET_LOADING;
  value: boolean;
};

export type ForecastAction =
  | UpdateForecastAction
  | ResetForecastAction
  | SetLoadingAction;
