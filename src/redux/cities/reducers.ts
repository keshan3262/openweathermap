import { Reducer } from 'redux';
import {
  CITIES_SET_LOADING,
  CitiesAction,
  CitiesState,
  DROP_INDEX,
  UPDATE_INDEX
} from './types';

export const initialState: CitiesState = {
  index: {},
  knownCities: {},
  loading: false
};

const citiesReducer: Reducer<CitiesState, CitiesAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case DROP_INDEX:
      return {
        ...state,
        index: {},
        knownCities: {}
      };
    case CITIES_SET_LOADING:
      return {
        ...state,
        loading: action.value
      };

    case UPDATE_INDEX: {
      const { cities, country, text } = action;

      const { index: prevIndex, knownCities: prevKnownCities } = state;

      return {
        ...state,
        index: {
          ...prevIndex,
          [country]: {
            ...prevIndex[country],
            [text]: cities.map(({ id }) => id)
          }
        },
        knownCities: cities.reduce(
          (newKnownCities, city) => ({
            ...newKnownCities,
            [city.id]: city
          }),
          { ...prevKnownCities }
        )
      };
    }

    default:
      return state;
  }
};

export default citiesReducer;
