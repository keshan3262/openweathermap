import { City } from '../../swagger/api';

export const UPDATE_INDEX = 'UPDATE_INDEX';
export const DROP_INDEX = 'DROP_INDEX';
export const CITIES_SET_LOADING = 'CITIES_SET_LOADING';

export type CitiesState = {
  index: {
    [country: string]: {
      [text: string]: number[];
    };
  };
  knownCities: {
    [id: number]: City;
  };
  loading: boolean;
};

export type UpdateIndexAction = {
  cities: City[];
  country: string;
  text: string;
  type: typeof UPDATE_INDEX;
};

export type DropIndexAction = {
  type: typeof DROP_INDEX;
};

export type SetLoadingAction = {
  type: typeof CITIES_SET_LOADING;
  value: boolean;
};

export type CitiesAction =
  | UpdateIndexAction
  | DropIndexAction
  | SetLoadingAction;
