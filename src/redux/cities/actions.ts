import { ThunkAction } from 'redux-thunk';
import { City } from '../../swagger/api';
import { setError } from '../error/actions';
import { ErrorAction } from '../error/types';
import { RootState } from '../store';
import {
  CITIES_SET_LOADING,
  CitiesAction,
  DROP_INDEX,
  DropIndexAction,
  SetLoadingAction,
  UPDATE_INDEX,
  UpdateIndexAction
} from './types';

export const dropIndex = (): DropIndexAction => ({
  type: DROP_INDEX
});

export const setLoading = (value: boolean): SetLoadingAction => ({
  type: CITIES_SET_LOADING,
  value
});

export const updateIndex = (
  cities: City[],
  country: string,
  text: string
): UpdateIndexAction => ({
  cities,
  country,
  text,
  type: UPDATE_INDEX
});

export const getCities = (
  country: string,
  name: string
): ThunkAction<void, RootState, [], CitiesAction | ErrorAction> => {
  return async (dispatch, getState) => {
    try {
      const {
        api: { citiesControllerApi },
        auth: { userData },
        cities: { index: citiesIndex }
      } = getState();

      if (!userData) {
        throw new Error('Authorization required');
      }

      const countryIndex = citiesIndex[country];

      if (countryIndex && countryIndex[name]) {
        return;
      }

      dispatch(setLoading(true));

      const cities = await citiesControllerApi.apiCitiesGet(
        country,
        name,
        undefined,
        { headers: { Authorization: userData.token } }
      );

      dispatch(updateIndex(cities, country, name));
    } catch (error) {
      dispatch(setError(error));
    } finally {
      dispatch(setLoading(false));
    }
  };
};
