import {
  applyMiddleware,
  combineReducers,
  createStore,
  Middleware
} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { reducer as formReducer, FormStateMap } from 'redux-form';
import thunk from 'redux-thunk';
import authReducer from './auth/reducers';
import { SignInAction, AuthState } from './auth/types';
import apiReducer from './api/reducers';
import { ApiState } from './api/types';
import citiesReducer from './cities/reducers';
import { CitiesState } from './cities/types';
import errorReducer from './error/reducers';
import { ErrorState } from './error/types';
import forecastReducer from './forecast/reducers';
import { ForecastState } from './forecast/types';

export type Action = SignInAction;

export type RootState = {
  api: ApiState;
  auth: AuthState;
  cities: CitiesState;
  error: ErrorState;
  forecast: ForecastState;
  form: FormStateMap;
};

const middlewares: Middleware[] = [thunk];

const makeStore = (initialState?: RootState) =>
  createStore(
    combineReducers({
      api: apiReducer,
      auth: authReducer,
      cities: citiesReducer,
      error: errorReducer,
      forecast: forecastReducer,
      form: formReducer
    }),
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares))
  );

export default makeStore;
