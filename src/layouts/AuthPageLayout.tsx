import {
  Button,
  Container,
  Link,
  LinkProps as MaterialLinkProps,
  Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import NextLink from 'next/link';
import React, { useCallback, useMemo } from 'react';
import { FormSubmitHandler, InjectedFormProps, reduxForm } from 'redux-form';
import * as Yup from 'yup';
import { makeValidateSyncFn } from '../utils/form';

type LinkProps = Omit<MaterialLinkProps, 'href'> & {
  href: string;
};

export type AuthPageLayoutProps<T extends {}> = {
  children: React.ReactNode;
  formId: string;
  initialValues: T;
  links: LinkProps[];
  onSubmit: FormSubmitHandler<T>;
  submitText: string;
  title: string;
  validationSchema: Yup.ObjectSchema;
};

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    alignItems: 'center',
    textAlign: 'center'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  links: {
    textAlign: 'left'
  },
  submit: {
    margin: theme.spacing(2, 0, 2)
  }
}));

export const AuthPageLayout = <T extends {}>(props: AuthPageLayoutProps<T>) => {
  const {
    children,
    formId,
    initialValues,
    links,
    onSubmit,
    submitText,
    title,
    validationSchema
  } = props;

  const classes = useStyles();

  const PageForm = useCallback(
    (formProps: InjectedFormProps<T>) => {
      const { handleSubmit, valid } = formProps;

      return (
        <form className={classes.form} onSubmit={handleSubmit}>
          {children}

          <Button
            className={classes.submit}
            color="primary"
            disabled={!valid}
            fullWidth={true}
            type="submit"
            variant="contained"
          >
            {submitText}
          </Button>
        </form>
      );
    },
    [children, classes, submitText]
  );

  const ConnectedPageForm = useMemo(
    () =>
      reduxForm<T>({
        form: formId,
        validate: makeValidateSyncFn(validationSchema)
      })(PageForm),
    [formId, PageForm, validationSchema]
  );

  return (
    <Container className={classes.paper} component="main" maxWidth="xs">
      <Typography component="h1" variant="h4">
        {title}
      </Typography>

      <ConnectedPageForm initialValues={initialValues} onSubmit={onSubmit} />

      <div className={classes.links}>
        {links.map(linkProps => (
          <NextLink href={linkProps.href} key={linkProps.href}>
            <Link {...linkProps} />
          </NextLink>
        ))}
      </div>
    </Container>
  );
};
