import React from 'react';
import { FormSubmitHandler } from 'redux-form';
import * as Yup from 'yup';
import { AuthPageLayout } from './AuthPageLayout';
import { ConnectedTextField } from '../components/ConnectedTextField';
import { signUpPath } from '../constants/routes';

export type SignInCredentials = {
  email: string;
  password: string;
};

export type SignInPageLayoutProps = {
  initialValues?: SignInCredentials;
  onSubmit: FormSubmitHandler<SignInCredentials>;
};

const credentialsSchema = Yup.object().shape({
  email: Yup.string()
    .email('Should be valid email address')
    .required('Required'),
  password: Yup.string().required('Required')
});

const defaultInititalValues = {
  email: '',
  password: ''
};

const links = [
  { children: "Don't have an account? Sign up", href: signUpPath }
];

export const SignInPageLayout = (props: SignInPageLayoutProps) => {
  const { initialValues = defaultInititalValues, onSubmit } = props;

  return (
    <AuthPageLayout
      formId="signInForm"
      initialValues={initialValues}
      links={links}
      onSubmit={onSubmit}
      submitText="Sign in"
      title="Sign in"
      validationSchema={credentialsSchema}
    >
      <ConnectedTextField
        autoComplete="email"
        label="Email"
        name="email"
        required={true}
      />

      <ConnectedTextField
        autoComplete="password"
        label="Password"
        name="password"
        required={true}
        type="password"
      />
    </AuthPageLayout>
  );
};
