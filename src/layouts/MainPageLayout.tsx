import { Container, Link } from '@material-ui/core';
import React from 'react';
import { PageWrapper } from '../components/PageWrapper';
import { QuestionAnswer } from '../components/QuestionAnswer';

export const MainPageLayout = React.memo(function MainPageLayout() {
  return (
    <PageWrapper route="/">
      <Container maxWidth="md">
        <QuestionAnswer question="What is it?">
          It&apos;s a full stack MERN application which provides weather
          forecast.
        </QuestionAnswer>

        <QuestionAnswer question="Why was it created?">
          It was created for learning purposes. The author hopes to become a{' '}
          frontend Next.js developer and to become a full-stack developer later.
        </QuestionAnswer>

        <QuestionAnswer question="Where is weather forecast taken from?">
          It&apos;s taken from{' '}
          <Link href="https://openweathermap.org/api">OpenWeather API</Link>
        </QuestionAnswer>
      </Container>
    </PageWrapper>
  );
});
