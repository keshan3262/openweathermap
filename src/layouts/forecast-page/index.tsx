import { Button, CircularProgress, Container, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  FormSubmitHandler,
  formValueSelector,
  InjectedFormProps,
  reduxForm
} from 'redux-form';
import * as Yup from 'yup';
import { CitySelect, CitySelectProps } from '../../components/CitySelect';
import { ConnectedSelect } from '../../components/ConnectedSelect';
import { CountrySelect } from '../../components/CountrySelect';
import { Units } from '../../swagger/api';
import { makeValidateSyncFn } from '../../utils/form';
import { RootState } from '../../redux/store';
import { getCities } from '../../redux/cities/actions';
import { resetForecast } from '../../redux/forecast/actions';
import { ForecastState } from '../../redux/forecast/types';
import { CurrentWeatherView } from './components/CurrentWeatherView';
import { DaysView } from './components/DaysView';
import { PageWrapper } from '../../components/PageWrapper';

export type FormValues = {
  city: number;
  country: string;
  units: Units;
};

export type ForecastPageLayoutProps = {
  initialValues?: FormValues;
  onChange: () => void;
  onSubmit: FormSubmitHandler<FormValues>;
};

const formId = 'forecastForm';

const validationSchema = Yup.object().shape({
  city: Yup.number().required('Required'),
  country: Yup.string().required('Required'),
  units: Yup.string()
    .default(Units.Metric)
    .oneOf(Object.values(Units))
});

const defaultInitialValues = {
  units: Units.Metric
};

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(1)
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    margin: theme.spacing(1, 0)
  },
  formGrid: {
    alignItems: 'end'
  },
  denseSelect: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(0.5)
  },
  submit: {
    marginBottom: theme.spacing(0.5)
  }
}));

const selector = formValueSelector<RootState>(formId);

const PageForm = (formProps: InjectedFormProps<FormValues>) => {
  const { handleSubmit, valid } = formProps;

  const [cityInputValue, setCityInputValue] = useState<string>();

  const classes = useStyles();
  const reduxState = useSelector<RootState, RootState>(state => state);
  const country = selector(reduxState, 'country');
  const dispatch = useDispatch();

  const {
    cities: { index: citiesIndex, knownCities, loading }
  } = reduxState;

  const countryIndex = citiesIndex[country];

  const citiesIds =
    cityInputValue != null && countryIndex && countryIndex[cityInputValue];

  const handleCitySelectInputChange = useCallback<
    CitySelectProps['onInputChange']
  >(
    newInputValue => {
      dispatch(resetForecast());
      setCityInputValue(newInputValue);

      if (country != null) {
        dispatch(getCities(country, newInputValue));
      }
    },
    [country, dispatch]
  );

  useEffect(() => {
    handleCitySelectInputChange('');
    dispatch(resetForecast());
  }, [country, dispatch, handleCitySelectInputChange]);

  const cities = useMemo(
    () => (citiesIds ? citiesIds.map(cityId => knownCities[cityId]) : []),
    [citiesIds, knownCities]
  );

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
      <Grid className={classes.formGrid} container={true} spacing={2}>
        <Grid item={true} xs={12} sm={4} md={4}>
          <label htmlFor="country">Country</label>

          <CountrySelect margin="dense" name="country" />
        </Grid>

        <Grid item={true} xs={12} sm={5} md={4}>
          <label htmlFor="city">City</label>

          <CitySelect
            disabled={!country}
            loading={loading}
            margin="dense"
            name="city"
            onInputChange={handleCitySelectInputChange}
            options={cities}
          />
        </Grid>

        <Grid item={true} xs={12} sm={3} md={2}>
          <ConnectedSelect
            className={classes.denseSelect}
            label="Units"
            name="units"
            margin="dense"
            native={true}
            showSeparateLabel={true}
          >
            <option value={Units.Metric}>Metric</option>
            <option value={Units.Imperial}>Imperial</option>
          </ConnectedSelect>
        </Grid>

        <Grid item={true} xs={12} sm={2}>
          <Button
            className={classes.submit}
            color="primary"
            disabled={!valid}
            fullWidth={true}
            size="large"
            type="submit"
            variant="contained"
          >
            Get
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

const ConnectedPageForm = reduxForm<FormValues>({
  form: formId,
  validate: makeValidateSyncFn(validationSchema)
})(PageForm);

export const ForecastPageLayout = (props: ForecastPageLayoutProps) => {
  const { initialValues = defaultInitialValues, onChange, onSubmit } = props;

  const classes = useStyles();

  const { forecast, loading } = useSelector<RootState, ForecastState>(
    state => state.forecast
  );

  return (
    <PageWrapper route="/forecast">
      <Container className={classes.paper} component="main" maxWidth="md">
        <ConnectedPageForm
          initialValues={initialValues}
          onChange={onChange}
          onSubmit={onSubmit}
        />

        {loading ? (
          <Grid container={true} justify="center">
            <Grid item={true} xs={12}>
              <CircularProgress />
            </Grid>
          </Grid>
        ) : (
          forecast && (
            <Grid container={true} spacing={2}>
              <Grid item={true} xs={12} md={3}>
                <CurrentWeatherView {...forecast.current} />
              </Grid>

              <Grid item={true} xs={12} md={9}>
                <DaysView days={forecast.days} />
              </Grid>
            </Grid>
          )
        )}
      </Container>
    </PageWrapper>
  );
};
