import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import { TransformedDailyWeatherDescription } from '../../../utils/transform-weather-forecast';

export type WeatherConditionIconProps = Pick<
  TransformedDailyWeatherDescription,
  'description' | 'icon'
>;

const useStyles = makeStyles(() => ({
  weatherConditionIcon: {
    display: 'inline-block',
    margin: '-0.6rem 0 -0.6rem -0.6rem',
    width: '3rem',
    height: '3rem'
  }
}));

const urlPrefix = 'https://openweathermap.org/img/wn';

export const WeatherConditionIcon = React.memo(function WeatherConditionIcon(
  props: WeatherConditionIconProps
) {
  const { description, icon } = props;

  const classes = useStyles();

  return (
    <img
      alt={description}
      className={classes.weatherConditionIcon}
      srcSet={`${urlPrefix}/${icon}.png 1x, ${urlPrefix}/${icon}@2x.png 2x`}
      title={description}
    />
  );
});
