import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelProps,
  ExpansionPanelSummary,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React, { useCallback } from 'react';
import { TransformedForecastDay } from '../../../utils/transform-weather-forecast';
import { BasicDayWeatherDescriptionView } from './BasicDayWeatherDescriptionView';
import { HourlyWeatherView } from './HourlyWeatherView';
import { NonVerboseTemperatureDescriptionView } from './NonVerboseTemperatureDescriptionView';
import { WeatherConditionIcon } from './WeatherConditionIcon';

export type DayViewProps = TransformedForecastDay & {
  expanded: boolean;
  onChange: (dayStr: string, isExpanded: boolean) => void;
};

export const DayView = (props: DayViewProps) => {
  const {
    day,
    dayStr,
    description,
    dt,
    expanded,
    eve,
    hourly,
    humidity,
    icon,
    maxTemp,
    minTemp,
    morn,
    night,
    onChange,
    pressure,
    sunrise,
    sunset,
    uvi,
    wind
  } = props;

  const handleChange = useCallback<
    NonNullable<ExpansionPanelProps['onChange']>
  >(
    (event, isExpanded) => {
      onChange(dayStr, isExpanded);
    },
    [dayStr, onChange]
  );

  return (
    <ExpansionPanel expanded={expanded} onChange={handleChange}>
      <ExpansionPanelSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls={`panel${dt.unix()}bh-content`}
        id={`panel${dt.unix()}bh-header`}
      >
        <Grid alignItems="center" container={true} spacing={1}>
          <Grid item={true} xs={3}>
            <Typography component="h2" variant="h6">
              {dayStr}
            </Typography>
          </Grid>

          <Grid item={true} xs={1}>
            <WeatherConditionIcon description={description} icon={icon} />
          </Grid>

          <Grid item={true} xs={4}>
            <Typography variant="body1">
              Min: {minTemp} Max: {maxTemp}
            </Typography>
          </Grid>

          <Grid item={true} xs={4}>
            <Typography variant="body1">Wind: {wind}</Typography>
          </Grid>
        </Grid>
      </ExpansionPanelSummary>

      <ExpansionPanelDetails>
        <Grid container={true} spacing={1}>
          <Grid item={true} xs={12}>
            <TableContainer>
              <Table aria-label="table of temperatures for day parts">
                <TableHead>
                  <TableRow>
                    <TableCell>Day part</TableCell>
                    <TableCell>Temperature</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Morning
                    </TableCell>

                    <TableCell>
                      <NonVerboseTemperatureDescriptionView {...morn} />
                    </TableCell>
                  </TableRow>

                  <TableRow>
                    <TableCell component="th" scope="row">
                      Day
                    </TableCell>

                    <TableCell>
                      <NonVerboseTemperatureDescriptionView {...day} />
                    </TableCell>
                  </TableRow>

                  <TableRow>
                    <TableCell component="th" scope="row">
                      Evening
                    </TableCell>

                    <TableCell>
                      <NonVerboseTemperatureDescriptionView {...eve} />
                    </TableCell>
                  </TableRow>

                  <TableRow>
                    <TableCell component="th" scope="row">
                      Night
                    </TableCell>

                    <TableCell>
                      <NonVerboseTemperatureDescriptionView {...night} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>

          <Grid item={true} xs={12}>
            <BasicDayWeatherDescriptionView
              humidity={humidity}
              pressure={pressure}
              sunrise={sunrise}
              sunset={sunset}
              uvi={uvi}
            />
          </Grid>

          <Grid item={true} xs={12}>
            <HourlyWeatherView items={hourly} />
          </Grid>
        </Grid>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};
