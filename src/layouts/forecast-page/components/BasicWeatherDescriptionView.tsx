import { Typography } from '@material-ui/core';
import React from 'react';
import { TransformedBasicWeatherDescription } from '../../../utils/transform-weather-forecast';

export type BasicWeatherDescriptionViewProps = Pick<
  TransformedBasicWeatherDescription,
  'humidity' | 'pressure'
> & {
  wind?: TransformedBasicWeatherDescription['wind'];
};

export const BasicWeatherDescriptionView = React.memo(
  function BasicWeatherDescriptionView(
    props: BasicWeatherDescriptionViewProps
  ) {
    const { humidity, pressure, wind } = props;

    return (
      <React.Fragment>
        {wind && <Typography variant="body1">Wind: {wind}</Typography>}

        <Typography variant="body1">Humidity: {humidity}</Typography>

        <Typography variant="body1">Pressure: {pressure}</Typography>
      </React.Fragment>
    );
  }
);
