import { Typography } from '@material-ui/core';
import React from 'react';
import { TransformedNonVerboseTemperatureDescription } from '../../../utils/transform-weather-forecast';

export type NonVerboseTemperatureDescriptionViewProps = TransformedNonVerboseTemperatureDescription;

export const NonVerboseTemperatureDescriptionView = React.memo(
  function NonVerboseTemperatureDescriptionView(
    props: NonVerboseTemperatureDescriptionViewProps
  ) {
    const { feelsLike, temp } = props;

    return (
      <Typography component="span" variant="body1">
        {temp}, feels like {feelsLike}
      </Typography>
    );
  }
);
