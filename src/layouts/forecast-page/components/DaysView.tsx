import { Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import useExpansionsState from '../../../hooks/useExpansionsState';
import { TransformedForecast } from '../../../utils/transform-weather-forecast';
import { DayView } from './DayView';

export type DaysViewProps = {
  days: TransformedForecast['days'];
  expandedItemsKeys?: string[];
};

export const DaysView = React.memo(function DaysView(props: DaysViewProps) {
  const { days, expandedItemsKeys: forcedExpandedItemsKeys } = props;

  const {
    expandedItemsKeys: expandedDays,
    handleExpansionChange,
    setExpandedItemsKeys
  } = useExpansionsState();

  useEffect(() => {
    if (forcedExpandedItemsKeys != null) {
      setExpandedItemsKeys(forcedExpandedItemsKeys);
    }
  }, [forcedExpandedItemsKeys, setExpandedItemsKeys]);

  return (
    <div>
      <Typography component="h1" gutterBottom={true} variant="h5">
        Weather forecast
      </Typography>

      {days.map(({ dayStr, ...restDayProps }) => (
        <DayView
          {...restDayProps}
          dayStr={dayStr}
          expanded={expandedDays.includes(dayStr)}
          key={dayStr}
          onChange={handleExpansionChange}
        />
      ))}
    </div>
  );
});
