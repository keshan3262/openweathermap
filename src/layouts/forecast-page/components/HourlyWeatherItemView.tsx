import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelProps,
  ExpansionPanelSummary,
  Grid,
  Typography
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React, { useCallback } from 'react';
import { TransformedHourlyWeatherDescription } from '../../../utils/transform-weather-forecast';
import { BasicWeatherDescriptionView } from './BasicWeatherDescriptionView';
import { NonVerboseTemperatureDescriptionView } from './NonVerboseTemperatureDescriptionView';
import { WeatherConditionIcon } from './WeatherConditionIcon';

export type HourlyWeatherItemViewProps = TransformedHourlyWeatherDescription & {
  expanded: boolean;
  onChange: (dayStr: string, isExpanded: boolean) => void;
};

export const HourlyWeatherItemView = React.memo(function HourlyWeatherItemView(
  props: HourlyWeatherItemViewProps
) {
  const {
    description,
    dt,
    expanded,
    feelsLike,
    humidity,
    icon,
    onChange,
    pressure,
    temp,
    wind
  } = props;

  const time = dt.format('HH:mm');

  const handleChange = useCallback<
    NonNullable<ExpansionPanelProps['onChange']>
  >(
    (event, isExpanded) => {
      onChange(time, isExpanded);
    },
    [onChange, time]
  );

  /* jscpd:ignore-start */
  return (
    <ExpansionPanel expanded={expanded} onChange={handleChange}>
      <ExpansionPanelSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls={`panel${dt.unix()}bh-content`}
        id={`panel${dt.unix()}bh-header`}
      >
        {/* jscpd:ignore-end */}
        <Grid alignItems="center" container={true} spacing={1}>
          <Grid item={true} xs={2}>
            <Typography component="h3" variant="h6">
              {time}
            </Typography>
          </Grid>

          <Grid item={true} xs={1}>
            <WeatherConditionIcon description={description} icon={icon} />
          </Grid>

          <Grid item={true} xs={5}>
            <NonVerboseTemperatureDescriptionView
              feelsLike={feelsLike}
              temp={temp}
            />
          </Grid>

          <Grid item={true} xs={4}>
            <Typography variant="body1">Wind: {wind}</Typography>
          </Grid>
        </Grid>
      </ExpansionPanelSummary>

      <ExpansionPanelDetails>
        <Grid container={true}>
          <Grid item={true} xs={12}>
            <BasicWeatherDescriptionView
              humidity={humidity}
              pressure={pressure}
            />
          </Grid>
        </Grid>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
});
