import { Typography } from '@material-ui/core';
import React from 'react';
import { TransformedCurrentWeatherDescription } from '../../../utils/transform-weather-forecast';
import { WeatherConditionIcon } from './WeatherConditionIcon';
import { BasicDayWeatherDescriptionView } from './BasicDayWeatherDescriptionView';
import { NonVerboseTemperatureDescriptionView } from './NonVerboseTemperatureDescriptionView';

export type CurrentWeatherViewProps = TransformedCurrentWeatherDescription;

export const CurrentWeatherView = React.memo(function CurrentWeatherView(
  props: CurrentWeatherViewProps
) {
  const {
    description,
    humidity,
    icon,
    pressure,
    wind,
    sunrise,
    sunset,
    uvi,
    feelsLike,
    temp
  } = props;

  return (
    <React.Fragment>
      <Typography component="h1" gutterBottom={true} variant="h5">
        Current weather
      </Typography>

      <WeatherConditionIcon description={description} icon={icon} />

      <NonVerboseTemperatureDescriptionView feelsLike={feelsLike} temp={temp} />

      <BasicDayWeatherDescriptionView
        humidity={humidity}
        pressure={pressure}
        sunrise={sunrise}
        sunset={sunset}
        uvi={uvi}
        wind={wind}
      />
    </React.Fragment>
  );
});
