import { Typography } from '@material-ui/core';
import React from 'react';
import { TransformedBasicDayWeatherDescription } from '../../../utils/transform-weather-forecast';
import { BasicWeatherDescriptionView } from './BasicWeatherDescriptionView';

export type BasicDayWeatherDescriptionViewProps = Omit<
  TransformedBasicDayWeatherDescription,
  'description' | 'dt' | 'icon' | 'wind'
> & {
  wind?: TransformedBasicDayWeatherDescription['wind'];
};

export const BasicDayWeatherDescriptionView = React.memo(
  function BasicDayWeatherDescriptionView(
    props: BasicDayWeatherDescriptionViewProps
  ) {
    const { humidity, pressure, sunrise, sunset, uvi, wind } = props;

    return (
      <React.Fragment>
        <BasicWeatherDescriptionView
          humidity={humidity}
          pressure={pressure}
          wind={wind}
        />

        <Typography variant="body1">
          Sunrise: {sunrise}, sunset: {sunset}
        </Typography>

        <Typography variant="body1">UVI index: {uvi}</Typography>
      </React.Fragment>
    );
  }
);
