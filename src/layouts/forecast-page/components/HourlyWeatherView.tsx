import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React, { useState, useCallback, useEffect } from 'react';
import useExpansionsState from '../../../hooks/useExpansionsState';
import { TransformedHourlyWeatherDescription } from '../../../utils/transform-weather-forecast';
import { HourlyWeatherItemView } from './HourlyWeatherItemView';

export type HourlyWeatherViewProps = {
  expandedItemsKeys?: string[];
  items: TransformedHourlyWeatherDescription[];
  showHourlyForecast?: boolean;
};

const useStyles = makeStyles(theme => ({
  button: {
    marginBottom: theme.spacing(1)
  }
}));

export const HourlyWeatherView = (props: HourlyWeatherViewProps) => {
  const {
    expandedItemsKeys: forcedExpandedItemsKeys,
    items,
    showHourlyForecast: forcedShowHourlyForecast
  } = props;

  const classes = useStyles();

  const [showForecast, setShowForecast] = useState(false);

  const handleShowForecastToggle = useCallback(() => {
    setShowForecast(!showForecast);
  }, [showForecast, setShowForecast]);

  const {
    expandedItemsKeys,
    handleExpansionChange,
    setExpandedItemsKeys
  } = useExpansionsState();

  useEffect(() => {
    if (forcedExpandedItemsKeys != null) {
      setExpandedItemsKeys(forcedExpandedItemsKeys);
    }
  }, [forcedExpandedItemsKeys, setExpandedItemsKeys]);

  useEffect(() => {
    if (forcedShowHourlyForecast != null) {
      setShowForecast(forcedShowHourlyForecast);
    }
  }, [forcedShowHourlyForecast]);

  if (items.length === 0) {
    return null;
  }

  return (
    <React.Fragment>
      <Button
        className={classes.button}
        color="primary"
        onClick={handleShowForecastToggle}
        variant="contained"
      >
        {showForecast ? 'Hide hourly forecast' : 'Show hourly forecast'}
      </Button>

      {showForecast && (
        <React.Fragment>
          {items.map(({ dt, ...restItemProps }) => (
            <HourlyWeatherItemView
              {...restItemProps}
              expanded={expandedItemsKeys.includes(dt.format('HH:mm'))}
              key={dt.format('HH:mm')}
              dt={dt}
              onChange={handleExpansionChange}
            />
          ))}
        </React.Fragment>
      )}
    </React.Fragment>
  );
};
