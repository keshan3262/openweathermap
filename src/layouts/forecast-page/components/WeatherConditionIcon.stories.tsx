import { storiesOf } from '@storybook/react';
import React from 'react';
import { WeatherConditionIcon } from './WeatherConditionIcon';

const iconsNames: Record<string, string> = {
  '01': 'clear sky',
  '02': 'few clouds',
  '03': 'scattered clouds',
  '04': 'broken clouds',
  '09': 'shower rain',
  '10': 'rain',
  '11': 'thunderstorm',
  '13': 'snow',
  '50': 'mist'
};

const stories = storiesOf('components/WeatherConditionIcon', module);

Object.keys(iconsNames).forEach(iconCode =>
  stories
    .add(`${iconsNames[iconCode]}`, () => (
      <WeatherConditionIcon
        description={iconsNames[iconCode]}
        icon={`${iconCode}d`}
      />
    ))
    .add(`${iconsNames[iconCode]} (night)`, () => (
      <WeatherConditionIcon
        description={iconsNames[iconCode]}
        icon={`${iconCode}n`}
      />
    ))
);
