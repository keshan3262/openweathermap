import { storiesOf } from '@storybook/react';
import React from 'react';
import { HourlyWeatherView } from './HourlyWeatherView';
import { transformedMetricWeatherForecast } from '../../../test-data/weather-forecast';

const items = transformedMetricWeatherForecast.days[0].hourly;

const allItemsKeys = items.map(item => item.dt.format('HH:mm'));

storiesOf('components/HourlyWeatherView[NO-JEST-STORYSHOT]', module)
  .add('default state', () => <HourlyWeatherView items={items} />)
  .add('items are shown but closed', () => (
    <HourlyWeatherView items={items} showHourlyForecast={true} />
  ))
  .add('all items are opened', () => (
    <HourlyWeatherView
      expandedItemsKeys={allItemsKeys}
      items={items}
      showHourlyForecast={true}
    />
  ));
