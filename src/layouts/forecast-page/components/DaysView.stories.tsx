import { storiesOf } from '@storybook/react';
import React from 'react';
import { transformedMetricWeatherForecast } from '../../../test-data/weather-forecast';
import { DaysView } from './DaysView';

const testDays = transformedMetricWeatherForecast.days;

storiesOf('components/DaysView', module)
  .add('default state', () => <DaysView days={testDays} />)
  .add('all days are expanded', () => (
    <DaysView
      days={testDays}
      expandedItemsKeys={testDays.map(({ dayStr }) => dayStr)}
    />
  ));
