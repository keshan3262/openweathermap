import { storiesOf } from '@storybook/react';
import React from 'react';
import { transformedMetricWeatherForecast } from '../../../test-data/weather-forecast';
import { CurrentWeatherView } from './CurrentWeatherView';

storiesOf('components/CurrentWeatherView', module).add('default state', () => (
  <CurrentWeatherView {...transformedMetricWeatherForecast.current} />
));
