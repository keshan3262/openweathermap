import React from 'react';
import { FormSubmitHandler } from 'redux-form';
import * as Yup from 'yup';
import { AuthPageLayout } from './AuthPageLayout';
import { ConnectedTextField } from '../components/ConnectedTextField';
import { signInPath } from '../constants/routes';

export type SignUpCredentials = {
  email: string;
  nickname: string;
  password: string;
  repeatPassword: string;
};

export type SignUpPageLayoutProps = {
  initialValues?: SignUpCredentials;
  onSubmit: FormSubmitHandler<SignUpCredentials>;
};

const credentialsSchema = Yup.object().shape({
  email: Yup.string()
    .email('Should be valid email address')
    .required('Required'),
  nickname: Yup.string().required('Required'),
  password: Yup.string()
    .matches(
      /[A-z0-9!@#$%^&*]{8,30}/,
      'Password should have from 8 to 30 characters, only alphanumeric and \
!@#$%^&* characters are allowed'
    )
    .required('Required'),
  repeatPassword: Yup.mixed().when('password', (value: string) =>
    Yup.string()
      .oneOf([value], 'Passwords should match')
      .required('Required')
  )
});

const defaultInititalValues = {
  email: '',
  nickname: '',
  password: '',
  repeatPassword: ''
};

const links = [{ children: 'Have an account? Sign in', href: signInPath }];

export const SignUpPageLayout = (props: SignUpPageLayoutProps) => {
  const { initialValues = defaultInititalValues, onSubmit } = props;

  return (
    <AuthPageLayout
      formId="signUpForm"
      initialValues={initialValues}
      links={links}
      onSubmit={onSubmit}
      submitText="Sign up"
      title="Sign up"
      validationSchema={credentialsSchema}
    >
      <ConnectedTextField
        autoComplete="email"
        label="Email"
        name="email"
        required={true}
        variant="outlined"
      />

      <ConnectedTextField
        autoComplete="nickname"
        label="Nickname"
        name="nickname"
        required={true}
      />

      <ConnectedTextField
        autoComplete="password"
        label="Password"
        name="password"
        required={true}
        type="password"
      />

      <ConnectedTextField
        autoComplete="password"
        label="Repeat password"
        name="repeatPassword"
        required={true}
        type="password"
      />
    </AuthPageLayout>
  );
};
