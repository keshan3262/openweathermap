import { Container, Link, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import NextLink from 'next/link';
import React from 'react';

const useStyles = makeStyles(() => ({
  container: {
    textAlign: 'center'
  }
}));

export const NotFoundLayout = () => {
  const classes = useStyles();

  return (
    <Container className={classes.container} maxWidth="md">
      <Typography component="h1" gutterBottom={true} variant="h3">
        Sorry, no such page was found
      </Typography>

      <NextLink href="/">
        <Link href="/">Go to home page</Link>
      </NextLink>
    </Container>
  );
};
