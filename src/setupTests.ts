import moment from 'moment-timezone';

// Tests results should be independent on timezone of the environment
moment.tz.setDefault('Greenwich');
