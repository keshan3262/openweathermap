import { pathToRegexp } from 'path-to-regexp';

export const forecastPath = '/forecast';
export const signInPath = '/signin';
export const signUpPath = '/signup';

export const paths: Record<string, { regex: RegExp; title: string }> = {
  '/': {
    regex: pathToRegexp('/'),
    title: 'Main page'
  },
  [forecastPath]: {
    regex: pathToRegexp(forecastPath),
    title: 'Weather forecast'
  },
  [signInPath]: {
    regex: pathToRegexp(signInPath),
    title: 'Sign in'
  },
  [signUpPath]: {
    regex: pathToRegexp(signUpPath),
    title: 'Sign up'
  }
};

export const pathsWithoutNavbar = [signInPath, signUpPath];
