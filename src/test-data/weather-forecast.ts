import moment from 'moment';
import { Forecast, Units } from '../swagger/api';
import { TransformedForecast } from '../utils/transform-weather-forecast';
import noTypeRawMetricWeatherForecast from './raw-weather-forecast.json';

export const rawMetricWeatherForecast = noTypeRawMetricWeatherForecast as Forecast;

export const rawImperialWeatherForecast: Forecast = {
  ...noTypeRawMetricWeatherForecast,
  units: Units.Imperial
};

export const transformedMetricWeatherForecast: TransformedForecast = {
  current: {
    description: 'overcast clouds',
    dt: moment(1587581315 * 1000),
    feelsLike: '+10\u00a0°C',
    humidity: '51\u00a0%',
    icon: '04n',
    pressure: '1005\u00a0hPa/754\u00a0mmHg',
    sunrise: '00:01 (06:01 +06)',
    sunset: '14:22 (20:22 +06)',
    temp: '+15\u00a0°C',
    uvi: 5.8,
    wind: 'S 6\u00a0m/s'
  },
  days: [
    {
      day: {
        feelsLike: '+6\u00a0°C',
        temp: '+12\u00a0°C'
      },
      dayStr: 'Thu, Apr 23',
      description: 'light rain',
      dt: moment(1587625200 * 1000),
      eve: {
        feelsLike: '+10\u00a0°C',
        temp: '+16\u00a0°C'
      },
      hourly: [
        {
          feelsLike: '+10\u00a0°C',
          temp: '+18\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587600000 * 1000),
          humidity: '30\u00a0%',
          icon: '04n',
          pressure: '1004\u00a0hPa/753\u00a0mmHg',
          wind: 'SSW 8\u00a0m/s'
        },
        {
          feelsLike: '+9\u00a0°C',
          temp: '+16\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587603600 * 1000),
          humidity: '47\u00a0%',
          icon: '04d',
          pressure: '1005\u00a0hPa/754\u00a0mmHg',
          wind: 'W 7\u00a0m/s'
        },
        {
          feelsLike: '+5\u00a0°C',
          temp: '+12\u00a0°C',
          description: 'light rain',
          dt: moment(1587607200 * 1000),
          humidity: '74\u00a0%',
          icon: '10d',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'WNW 9\u00a0m/s'
        },
        {
          feelsLike: '+5\u00a0°C',
          temp: '+10\u00a0°C',
          description: 'light rain',
          dt: moment(1587610800 * 1000),
          humidity: '71\u00a0%',
          icon: '10d',
          pressure: '1008\u00a0hPa/756\u00a0mmHg',
          wind: 'WNW 6\u00a0m/s'
        },
        {
          feelsLike: '+7\u00a0°C',
          temp: '+11\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587614400 * 1000),
          humidity: '68\u00a0%',
          icon: '04d',
          pressure: '1009\u00a0hPa/757\u00a0mmHg',
          wind: 'W 4\u00a0m/s'
        },
        {
          feelsLike: '+6\u00a0°C',
          temp: '+12\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587618000 * 1000),
          humidity: '61\u00a0%',
          icon: '04d',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'W 7\u00a0m/s'
        },
        {
          feelsLike: '+6\u00a0°C',
          temp: '+12\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587621600 * 1000),
          humidity: '55\u00a0%',
          icon: '04d',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'W 7\u00a0m/s'
        },
        {
          feelsLike: '+8\u00a0°C',
          temp: '+14\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587625200 * 1000),
          humidity: '46\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0m/s'
        },
        {
          feelsLike: '+9\u00a0°C',
          temp: '+15\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587628800 * 1000),
          humidity: '40\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0m/s'
        },
        {
          feelsLike: '+10\u00a0°C',
          temp: '+16\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587632400 * 1000),
          humidity: '35\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0m/s'
        },
        {
          feelsLike: '+10\u00a0°C',
          temp: '+16\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587636000 * 1000),
          humidity: '32\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0m/s'
        },
        {
          feelsLike: '+10\u00a0°C',
          temp: '+16\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587639600 * 1000),
          humidity: '31\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0m/s'
        },
        {
          feelsLike: '+10\u00a0°C',
          temp: '+16\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587643200 * 1000),
          humidity: '30\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 5\u00a0m/s'
        },
        {
          feelsLike: '+10\u00a0°C',
          temp: '+15\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587646800 * 1000),
          humidity: '31\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 4\u00a0m/s'
        },
        {
          feelsLike: '+11\u00a0°C',
          temp: '+14\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587650400 * 1000),
          humidity: '37\u00a0%',
          icon: '04d',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'W 2\u00a0m/s'
        },
        {
          feelsLike: '+10\u00a0°C',
          temp: '+13\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587654000 * 1000),
          humidity: '38\u00a0%',
          icon: '04n',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'NW 2\u00a0m/s'
        },
        {
          feelsLike: '+9\u00a0°C',
          temp: '+13\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587657600 * 1000),
          humidity: '39\u00a0%',
          icon: '04n',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'NNW 2\u00a0m/s'
        },
        {
          feelsLike: '+9\u00a0°C',
          temp: '+12\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587661200 * 1000),
          humidity: '40\u00a0%',
          icon: '04n',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'N 2\u00a0m/s'
        },
        {
          feelsLike: '+8\u00a0°C',
          temp: '+12\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587664800 * 1000),
          humidity: '42\u00a0%',
          icon: '04n',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'NNE 2\u00a0m/s'
        },
        {
          feelsLike: '+8\u00a0°C',
          temp: '+12\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587668400 * 1000),
          humidity: '42\u00a0%',
          icon: '04n',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'ENE 3\u00a0m/s'
        },
        {
          feelsLike: '+8\u00a0°C',
          temp: '+12\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587672000 * 1000),
          humidity: '43\u00a0%',
          icon: '04n',
          pressure: '1009\u00a0hPa/757\u00a0mmHg',
          wind: 'ENE 3\u00a0m/s'
        },
        {
          feelsLike: '+7\u00a0°C',
          temp: '+12\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587675600 * 1000),
          humidity: '45\u00a0%',
          icon: '04n',
          pressure: '1008\u00a0hPa/756\u00a0mmHg',
          wind: 'N 4\u00a0m/s'
        },
        {
          feelsLike: '+7\u00a0°C',
          temp: '+11\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587679200 * 1000),
          humidity: '45\u00a0%',
          icon: '04n',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'NNE 4\u00a0m/s'
        },
        {
          feelsLike: '+6\u00a0°C',
          temp: '+11\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587682800 * 1000),
          humidity: '47\u00a0%',
          icon: '04n',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'NNE 5\u00a0m/s'
        }
      ],
      humidity: '55\u00a0%',
      icon: '10d',
      maxTemp: '+17\u00a0°C',
      minTemp: '+10\u00a0°C',
      morn: {
        feelsLike: '+9\u00a0°C',
        temp: '+17\u00a0°C'
      },
      night: {
        feelsLike: '+8\u00a0°C',
        temp: '+12\u00a0°C'
      },
      pressure: '1010\u00a0hPa/758\u00a0mmHg',
      sunrise: '00:01 (06:01 +06)',
      sunset: '14:22 (20:22 +06)',
      uvi: 5.8,
      wind: 'W 7\u00a0m/s'
    },
    {
      day: {
        feelsLike: '+6\u00a0°C',
        temp: '+9\u00a0°C'
      },
      dayStr: 'Fri, Apr 24',
      description: 'heavy intensity rain',
      dt: moment(1587711600 * 1000),
      eve: {
        feelsLike: '+4\u00a0°C',
        temp: '+9\u00a0°C'
      },
      hourly: [
        {
          feelsLike: '+5\u00a0°C',
          temp: '+10\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587686400 * 1000),
          humidity: '53\u00a0%',
          icon: '04d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'N 5\u00a0m/s'
        },
        {
          feelsLike: '+3\u00a0°C',
          temp: '+8\u00a0°C',
          description: 'moderate rain',
          dt: moment(1587690000 * 1000),
          humidity: '74\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'NNW 6\u00a0m/s'
        },
        {
          feelsLike: '+4\u00a0°C',
          temp: '+8\u00a0°C',
          description: 'moderate rain',
          dt: moment(1587693600 * 1000),
          humidity: '79\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'NE 4\u00a0m/s'
        },
        {
          feelsLike: '+5\u00a0°C',
          temp: '+8\u00a0°C',
          description: 'moderate rain',
          dt: moment(1587697200 * 1000),
          humidity: '79\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'ENE 3\u00a0m/s'
        },
        {
          feelsLike: '+5\u00a0°C',
          temp: '+9\u00a0°C',
          description: 'moderate rain',
          dt: moment(1587700800 * 1000),
          humidity: '82\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'SE 3\u00a0m/s'
        },
        {
          feelsLike: '+6\u00a0°C',
          temp: '+9\u00a0°C',
          description: 'moderate rain',
          dt: moment(1587704400 * 1000),
          humidity: '82\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'S 2\u00a0m/s'
        },
        {
          feelsLike: '+6\u00a0°C',
          temp: '+9\u00a0°C',
          description: 'moderate rain',
          dt: moment(1587708000 * 1000),
          humidity: '83\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'SW 4\u00a0m/s'
        },
        {
          feelsLike: '+6\u00a0°C',
          temp: '+9\u00a0°C',
          description: 'moderate rain',
          dt: moment(1587711600 * 1000),
          humidity: '84\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'SW 4\u00a0m/s'
        },
        {
          feelsLike: '+6\u00a0°C',
          temp: '+10\u00a0°C',
          description: 'moderate rain',
          dt: moment(1587715200 * 1000),
          humidity: '82\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'SW 4\u00a0m/s'
        },
        {
          feelsLike: '+6\u00a0°C',
          temp: '+10\u00a0°C',
          description: 'light rain',
          dt: moment(1587718800 * 1000),
          humidity: '80\u00a0%',
          icon: '10d',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'WSW 5\u00a0m/s'
        },
        {
          feelsLike: '+3\u00a0°C',
          temp: '+8\u00a0°C',
          description: 'light rain',
          dt: moment(1587722400 * 1000),
          humidity: '78\u00a0%',
          icon: '10d',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'NW 5\u00a0m/s'
        },
        {
          feelsLike: '+5\u00a0°C',
          temp: '+8\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587726000 * 1000),
          humidity: '72\u00a0%',
          icon: '04d',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'W 3\u00a0m/s'
        },
        {
          feelsLike: '+4\u00a0°C',
          temp: '+9\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587729600 * 1000),
          humidity: '67\u00a0%',
          icon: '04d',
          pressure: '1008\u00a0hPa/756\u00a0mmHg',
          wind: 'W 5\u00a0m/s'
        },
        {
          feelsLike: '+5\u00a0°C',
          temp: '+9\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587733200 * 1000),
          humidity: '65\u00a0%',
          icon: '04d',
          pressure: '1008\u00a0hPa/756\u00a0mmHg',
          wind: 'W 4\u00a0m/s'
        },
        {
          feelsLike: '+5\u00a0°C',
          temp: '+9\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587736800 * 1000),
          humidity: '68\u00a0%',
          icon: '04d',
          pressure: '1009\u00a0hPa/757\u00a0mmHg',
          wind: 'SW 3\u00a0m/s'
        },
        {
          feelsLike: '+4\u00a0°C',
          temp: '+8\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587740400 * 1000),
          humidity: '69\u00a0%',
          icon: '04n',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'SW 4\u00a0m/s'
        },
        {
          feelsLike: '+4\u00a0°C',
          temp: '+8\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587744000 * 1000),
          humidity: '66\u00a0%',
          icon: '04n',
          pressure: '1012\u00a0hPa/759\u00a0mmHg',
          wind: 'W 4\u00a0m/s'
        },
        {
          feelsLike: '+3\u00a0°C',
          temp: '+8\u00a0°C',
          description: 'overcast clouds',
          dt: moment(1587747600 * 1000),
          humidity: '66\u00a0%',
          icon: '04n',
          pressure: '1013\u00a0hPa/760\u00a0mmHg',
          wind: 'W 4\u00a0m/s'
        }
      ],
      humidity: '83\u00a0%',
      icon: '10d',
      maxTemp: '+10\u00a0°C',
      minTemp: '+7\u00a0°C',
      morn: {
        feelsLike: '+5\u00a0°C',
        temp: '+10\u00a0°C'
      },
      night: {
        feelsLike: '+3\u00a0°C',
        temp: '+7\u00a0°C'
      },
      pressure: '1006\u00a0hPa/755\u00a0mmHg',
      sunrise: '23:59 (05:59 +06)',
      sunset: '14:24 (20:24 +06)',
      uvi: 5.92,
      wind: 'SW 4\u00a0m/s'
    },
    {
      day: {
        feelsLike: '+5\u00a0°C',
        temp: '+10\u00a0°C'
      },
      dayStr: 'Sat, Apr 25',
      description: 'overcast clouds',
      dt: moment(1587798000 * 1000),
      eve: {
        feelsLike: '+8\u00a0°C',
        temp: '+12\u00a0°C'
      },
      hourly: [],
      humidity: '40\u00a0%',
      icon: '04d',
      maxTemp: '+12\u00a0°C',
      minTemp: '+4\u00a0°C',
      morn: {
        feelsLike: '0\u00a0°C',
        temp: '+4\u00a0°C'
      },
      night: {
        feelsLike: '+4\u00a0°C',
        temp: '+8\u00a0°C'
      },
      pressure: '1021\u00a0hPa/766\u00a0mmHg',
      sunrise: '23:57 (05:57 +06)',
      sunset: '14:25 (20:25 +06)',
      uvi: 5.15,
      wind: 'WSW 4\u00a0m/s'
    },
    {
      day: {
        feelsLike: '+9\u00a0°C',
        temp: '+14\u00a0°C'
      },
      dayStr: 'Sun, Apr 26',
      description: 'broken clouds',
      dt: moment(1587884400 * 1000),
      eve: {
        feelsLike: '+12\u00a0°C',
        temp: '+16\u00a0°C'
      },
      hourly: [],
      humidity: '36\u00a0%',
      icon: '04d',
      maxTemp: '+16\u00a0°C',
      minTemp: '+6\u00a0°C',
      morn: {
        feelsLike: '+1\u00a0°C',
        temp: '+6\u00a0°C'
      },
      night: {
        feelsLike: '+6\u00a0°C',
        temp: '+10\u00a0°C'
      },
      pressure: '1021\u00a0hPa/766\u00a0mmHg',
      sunrise: '23:55 (05:55 +06)',
      sunset: '14:27 (20:27 +06)',
      uvi: 4.93,
      wind: 'E 5\u00a0m/s'
    },
    {
      day: {
        feelsLike: '+15\u00a0°C',
        temp: '+19\u00a0°C'
      },
      dayStr: 'Mon, Apr 27',
      description: 'light rain',
      dt: moment(1587970800 * 1000),
      eve: {
        feelsLike: '+15\u00a0°C',
        temp: '+18\u00a0°C'
      },
      hourly: [],
      humidity: '42\u00a0%',
      icon: '10d',
      maxTemp: '+21\u00a0°C',
      minTemp: '+8\u00a0°C',
      morn: {
        feelsLike: '+4\u00a0°C',
        temp: '+8\u00a0°C'
      },
      night: {
        feelsLike: '+14\u00a0°C',
        temp: '+20\u00a0°C'
      },
      pressure: '1013\u00a0hPa/760\u00a0mmHg',
      sunrise: '23:53 (05:53 +06)',
      sunset: '14:28 (20:28 +06)',
      uvi: 5.66,
      wind: 'SSE 4\u00a0m/s'
    },
    {
      day: {
        feelsLike: '+16\u00a0°C',
        temp: '+23\u00a0°C'
      },
      dayStr: 'Tue, Apr 28',
      description: 'light rain',
      dt: moment(1588057200 * 1000),
      eve: {
        feelsLike: '+16\u00a0°C',
        temp: '+20\u00a0°C'
      },
      hourly: [],
      humidity: '41\u00a0%',
      icon: '10d',
      maxTemp: '+23\u00a0°C',
      minTemp: '+16\u00a0°C',
      morn: {
        feelsLike: '+11\u00a0°C',
        temp: '+18\u00a0°C'
      },
      night: {
        feelsLike: '+11\u00a0°C',
        temp: '+16\u00a0°C'
      },
      pressure: '999\u00a0hPa/749\u00a0mmHg',
      sunrise: '23:52 (05:52 +06)',
      sunset: '14:30 (20:30 +06)',
      uvi: 5.97,
      wind: 'SW 10\u00a0m/s'
    },
    {
      day: {
        feelsLike: '+6\u00a0°C',
        temp: '+14\u00a0°C'
      },
      dayStr: 'Wed, Apr 29',
      description: 'light rain',
      dt: moment(1588143600 * 1000),
      eve: {
        feelsLike: '+9\u00a0°C',
        temp: '+15\u00a0°C'
      },
      hourly: [],
      humidity: '56\u00a0%',
      icon: '10d',
      maxTemp: '+16\u00a0°C',
      minTemp: '+10\u00a0°C',
      morn: {
        feelsLike: '+6\u00a0°C',
        temp: '+12\u00a0°C'
      },
      night: {
        feelsLike: '+6\u00a0°C',
        temp: '+10\u00a0°C'
      },
      pressure: '1010\u00a0hPa/758\u00a0mmHg',
      sunrise: '23:50 (05:50 +06)',
      sunset: '14:32 (20:32 +06)',
      uvi: 5.24,
      wind: 'W 10\u00a0m/s'
    },
    {
      day: {
        feelsLike: '+9\u00a0°C',
        temp: '+13\u00a0°C'
      },
      dayStr: 'Thu, Apr 30',
      description: 'few clouds',
      dt: moment(1588230000 * 1000),
      eve: {
        feelsLike: '+9\u00a0°C',
        temp: '+13\u00a0°C'
      },
      hourly: [],
      humidity: '37\u00a0%',
      icon: '02d',
      maxTemp: '+13\u00a0°C',
      minTemp: '+7\u00a0°C',
      morn: {
        feelsLike: '+4\u00a0°C',
        temp: '+7\u00a0°C'
      },
      night: {
        feelsLike: '+9\u00a0°C',
        temp: '+13\u00a0°C'
      },
      pressure: '1025\u00a0hPa/769\u00a0mmHg',
      sunrise: '23:48 (05:48 +06)',
      sunset: '14:33 (20:33 +06)',
      uvi: 5.37,
      wind: 'WNW 3\u00a0m/s'
    }
  ]
};

export const transformedImperialWeatherForecast: TransformedForecast = {
  current: {
    description: 'overcast clouds',
    dt: moment(1587581315 * 1000),
    feelsLike: '+10\u00a0°F',
    humidity: '51\u00a0%',
    icon: '04n',
    pressure: '1005\u00a0hPa/754\u00a0mmHg',
    sunrise: '00:01 (06:01 +06)',
    sunset: '14:22 (20:22 +06)',
    temp: '+15\u00a0°F',
    uvi: 5.8,
    wind: 'S 6\u00a0mph'
  },
  days: [
    {
      day: {
        feelsLike: '+6\u00a0°F',
        temp: '+12\u00a0°F'
      },
      dayStr: 'Thu, Apr 23',
      description: 'light rain',
      dt: moment(1587625200 * 1000),
      eve: {
        feelsLike: '+10\u00a0°F',
        temp: '+16\u00a0°F'
      },
      hourly: [
        {
          feelsLike: '+10\u00a0°F',
          temp: '+18\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587600000 * 1000),
          humidity: '30\u00a0%',
          icon: '04n',
          pressure: '1004\u00a0hPa/753\u00a0mmHg',
          wind: 'SSW 8\u00a0mph'
        },
        {
          feelsLike: '+9\u00a0°F',
          temp: '+16\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587603600 * 1000),
          humidity: '47\u00a0%',
          icon: '04d',
          pressure: '1005\u00a0hPa/754\u00a0mmHg',
          wind: 'W 7\u00a0mph'
        },
        {
          feelsLike: '+5\u00a0°F',
          temp: '+12\u00a0°F',
          description: 'light rain',
          dt: moment(1587607200 * 1000),
          humidity: '74\u00a0%',
          icon: '10d',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'WNW 9\u00a0mph'
        },
        {
          feelsLike: '+5\u00a0°F',
          temp: '+10\u00a0°F',
          description: 'light rain',
          dt: moment(1587610800 * 1000),
          humidity: '71\u00a0%',
          icon: '10d',
          pressure: '1008\u00a0hPa/756\u00a0mmHg',
          wind: 'WNW 6\u00a0mph'
        },
        {
          feelsLike: '+7\u00a0°F',
          temp: '+11\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587614400 * 1000),
          humidity: '68\u00a0%',
          icon: '04d',
          pressure: '1009\u00a0hPa/757\u00a0mmHg',
          wind: 'W 4\u00a0mph'
        },
        {
          feelsLike: '+6\u00a0°F',
          temp: '+12\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587618000 * 1000),
          humidity: '61\u00a0%',
          icon: '04d',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'W 7\u00a0mph'
        },
        {
          feelsLike: '+6\u00a0°F',
          temp: '+12\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587621600 * 1000),
          humidity: '55\u00a0%',
          icon: '04d',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'W 7\u00a0mph'
        },
        {
          feelsLike: '+8\u00a0°F',
          temp: '+14\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587625200 * 1000),
          humidity: '46\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0mph'
        },
        {
          feelsLike: '+9\u00a0°F',
          temp: '+15\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587628800 * 1000),
          humidity: '40\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0mph'
        },
        {
          feelsLike: '+10\u00a0°F',
          temp: '+16\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587632400 * 1000),
          humidity: '35\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0mph'
        },
        {
          feelsLike: '+10\u00a0°F',
          temp: '+16\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587636000 * 1000),
          humidity: '32\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0mph'
        },
        {
          feelsLike: '+10\u00a0°F',
          temp: '+16\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587639600 * 1000),
          humidity: '31\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 6\u00a0mph'
        },
        {
          feelsLike: '+10\u00a0°F',
          temp: '+16\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587643200 * 1000),
          humidity: '30\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 5\u00a0mph'
        },
        {
          feelsLike: '+10\u00a0°F',
          temp: '+15\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587646800 * 1000),
          humidity: '31\u00a0%',
          icon: '04d',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'WSW 4\u00a0mph'
        },
        {
          feelsLike: '+11\u00a0°F',
          temp: '+14\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587650400 * 1000),
          humidity: '37\u00a0%',
          icon: '04d',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'W 2\u00a0mph'
        },
        {
          feelsLike: '+10\u00a0°F',
          temp: '+13\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587654000 * 1000),
          humidity: '38\u00a0%',
          icon: '04n',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'NW 2\u00a0mph'
        },
        {
          feelsLike: '+9\u00a0°F',
          temp: '+13\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587657600 * 1000),
          humidity: '39\u00a0%',
          icon: '04n',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'NNW 2\u00a0mph'
        },
        {
          feelsLike: '+9\u00a0°F',
          temp: '+12\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587661200 * 1000),
          humidity: '40\u00a0%',
          icon: '04n',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'N 2\u00a0mph'
        },
        {
          feelsLike: '+8\u00a0°F',
          temp: '+12\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587664800 * 1000),
          humidity: '42\u00a0%',
          icon: '04n',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'NNE 2\u00a0mph'
        },
        {
          feelsLike: '+8\u00a0°F',
          temp: '+12\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587668400 * 1000),
          humidity: '42\u00a0%',
          icon: '04n',
          pressure: '1010\u00a0hPa/758\u00a0mmHg',
          wind: 'ENE 3\u00a0mph'
        },
        {
          feelsLike: '+8\u00a0°F',
          temp: '+12\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587672000 * 1000),
          humidity: '43\u00a0%',
          icon: '04n',
          pressure: '1009\u00a0hPa/757\u00a0mmHg',
          wind: 'ENE 3\u00a0mph'
        },
        {
          feelsLike: '+7\u00a0°F',
          temp: '+12\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587675600 * 1000),
          humidity: '45\u00a0%',
          icon: '04n',
          pressure: '1008\u00a0hPa/756\u00a0mmHg',
          wind: 'N 4\u00a0mph'
        },
        {
          feelsLike: '+7\u00a0°F',
          temp: '+11\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587679200 * 1000),
          humidity: '45\u00a0%',
          icon: '04n',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'NNE 4\u00a0mph'
        },
        {
          feelsLike: '+6\u00a0°F',
          temp: '+11\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587682800 * 1000),
          humidity: '47\u00a0%',
          icon: '04n',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'NNE 5\u00a0mph'
        }
      ],
      humidity: '55\u00a0%',
      icon: '10d',
      maxTemp: '+17\u00a0°F',
      minTemp: '+10\u00a0°F',
      morn: {
        feelsLike: '+9\u00a0°F',
        temp: '+17\u00a0°F'
      },
      night: {
        feelsLike: '+8\u00a0°F',
        temp: '+12\u00a0°F'
      },
      pressure: '1010\u00a0hPa/758\u00a0mmHg',
      sunrise: '00:01 (06:01 +06)',
      sunset: '14:22 (20:22 +06)',
      uvi: 5.8,
      wind: 'W 7\u00a0mph'
    },
    {
      day: {
        feelsLike: '+6\u00a0°F',
        temp: '+9\u00a0°F'
      },
      dayStr: 'Fri, Apr 24',
      description: 'heavy intensity rain',
      dt: moment(1587711600 * 1000),
      eve: {
        feelsLike: '+4\u00a0°F',
        temp: '+9\u00a0°F'
      },
      hourly: [
        {
          feelsLike: '+5\u00a0°F',
          temp: '+10\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587686400 * 1000),
          humidity: '53\u00a0%',
          icon: '04d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'N 5\u00a0mph'
        },
        {
          feelsLike: '+3\u00a0°F',
          temp: '+8\u00a0°F',
          description: 'moderate rain',
          dt: moment(1587690000 * 1000),
          humidity: '74\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'NNW 6\u00a0mph'
        },
        {
          feelsLike: '+4\u00a0°F',
          temp: '+8\u00a0°F',
          description: 'moderate rain',
          dt: moment(1587693600 * 1000),
          humidity: '79\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'NE 4\u00a0mph'
        },
        {
          feelsLike: '+5\u00a0°F',
          temp: '+8\u00a0°F',
          description: 'moderate rain',
          dt: moment(1587697200 * 1000),
          humidity: '79\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'ENE 3\u00a0mph'
        },
        {
          feelsLike: '+5\u00a0°F',
          temp: '+9\u00a0°F',
          description: 'moderate rain',
          dt: moment(1587700800 * 1000),
          humidity: '82\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'SE 3\u00a0mph'
        },
        {
          feelsLike: '+6\u00a0°F',
          temp: '+9\u00a0°F',
          description: 'moderate rain',
          dt: moment(1587704400 * 1000),
          humidity: '82\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'S 2\u00a0mph'
        },
        {
          feelsLike: '+6\u00a0°F',
          temp: '+9\u00a0°F',
          description: 'moderate rain',
          dt: moment(1587708000 * 1000),
          humidity: '83\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'SW 4\u00a0mph'
        },
        {
          feelsLike: '+6\u00a0°F',
          temp: '+9\u00a0°F',
          description: 'moderate rain',
          dt: moment(1587711600 * 1000),
          humidity: '84\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'SW 4\u00a0mph'
        },
        {
          feelsLike: '+6\u00a0°F',
          temp: '+10\u00a0°F',
          description: 'moderate rain',
          dt: moment(1587715200 * 1000),
          humidity: '82\u00a0%',
          icon: '10d',
          pressure: '1006\u00a0hPa/755\u00a0mmHg',
          wind: 'SW 4\u00a0mph'
        },
        {
          feelsLike: '+6\u00a0°F',
          temp: '+10\u00a0°F',
          description: 'light rain',
          dt: moment(1587718800 * 1000),
          humidity: '80\u00a0%',
          icon: '10d',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'WSW 5\u00a0mph'
        },
        {
          feelsLike: '+3\u00a0°F',
          temp: '+8\u00a0°F',
          description: 'light rain',
          dt: moment(1587722400 * 1000),
          humidity: '78\u00a0%',
          icon: '10d',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'NW 5\u00a0mph'
        },
        {
          feelsLike: '+5\u00a0°F',
          temp: '+8\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587726000 * 1000),
          humidity: '72\u00a0%',
          icon: '04d',
          pressure: '1007\u00a0hPa/755\u00a0mmHg',
          wind: 'W 3\u00a0mph'
        },
        {
          feelsLike: '+4\u00a0°F',
          temp: '+9\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587729600 * 1000),
          humidity: '67\u00a0%',
          icon: '04d',
          pressure: '1008\u00a0hPa/756\u00a0mmHg',
          wind: 'W 5\u00a0mph'
        },
        {
          feelsLike: '+5\u00a0°F',
          temp: '+9\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587733200 * 1000),
          humidity: '65\u00a0%',
          icon: '04d',
          pressure: '1008\u00a0hPa/756\u00a0mmHg',
          wind: 'W 4\u00a0mph'
        },
        {
          feelsLike: '+5\u00a0°F',
          temp: '+9\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587736800 * 1000),
          humidity: '68\u00a0%',
          icon: '04d',
          pressure: '1009\u00a0hPa/757\u00a0mmHg',
          wind: 'SW 3\u00a0mph'
        },
        {
          feelsLike: '+4\u00a0°F',
          temp: '+8\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587740400 * 1000),
          humidity: '69\u00a0%',
          icon: '04n',
          pressure: '1011\u00a0hPa/758\u00a0mmHg',
          wind: 'SW 4\u00a0mph'
        },
        {
          feelsLike: '+4\u00a0°F',
          temp: '+8\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587744000 * 1000),
          humidity: '66\u00a0%',
          icon: '04n',
          pressure: '1012\u00a0hPa/759\u00a0mmHg',
          wind: 'W 4\u00a0mph'
        },
        {
          feelsLike: '+3\u00a0°F',
          temp: '+8\u00a0°F',
          description: 'overcast clouds',
          dt: moment(1587747600 * 1000),
          humidity: '66\u00a0%',
          icon: '04n',
          pressure: '1013\u00a0hPa/760\u00a0mmHg',
          wind: 'W 4\u00a0mph'
        }
      ],
      humidity: '83\u00a0%',
      icon: '10d',
      maxTemp: '+10\u00a0°F',
      minTemp: '+7\u00a0°F',
      morn: {
        feelsLike: '+5\u00a0°F',
        temp: '+10\u00a0°F'
      },
      night: {
        feelsLike: '+3\u00a0°F',
        temp: '+7\u00a0°F'
      },
      pressure: '1006\u00a0hPa/755\u00a0mmHg',
      sunrise: '23:59 (05:59 +06)',
      sunset: '14:24 (20:24 +06)',
      uvi: 5.92,
      wind: 'SW 4\u00a0mph'
    },
    {
      day: {
        feelsLike: '+5\u00a0°F',
        temp: '+10\u00a0°F'
      },
      dayStr: 'Sat, Apr 25',
      description: 'overcast clouds',
      dt: moment(1587798000 * 1000),
      eve: {
        feelsLike: '+8\u00a0°F',
        temp: '+12\u00a0°F'
      },
      hourly: [],
      humidity: '40\u00a0%',
      icon: '04d',
      maxTemp: '+12\u00a0°F',
      minTemp: '+4\u00a0°F',
      morn: {
        feelsLike: '0\u00a0°F',
        temp: '+4\u00a0°F'
      },
      night: {
        feelsLike: '+4\u00a0°F',
        temp: '+8\u00a0°F'
      },
      pressure: '1021\u00a0hPa/766\u00a0mmHg',
      sunrise: '23:57 (05:57 +06)',
      sunset: '14:25 (20:25 +06)',
      uvi: 5.15,
      wind: 'WSW 4\u00a0mph'
    },
    {
      day: {
        feelsLike: '+9\u00a0°F',
        temp: '+14\u00a0°F'
      },
      dayStr: 'Sun, Apr 26',
      description: 'broken clouds',
      dt: moment(1587884400 * 1000),
      eve: {
        feelsLike: '+12\u00a0°F',
        temp: '+16\u00a0°F'
      },
      hourly: [],
      humidity: '36\u00a0%',
      icon: '04d',
      maxTemp: '+16\u00a0°F',
      minTemp: '+6\u00a0°F',
      morn: {
        feelsLike: '+1\u00a0°F',
        temp: '+6\u00a0°F'
      },
      night: {
        feelsLike: '+6\u00a0°F',
        temp: '+10\u00a0°F'
      },
      pressure: '1021\u00a0hPa/766\u00a0mmHg',
      sunrise: '23:55 (05:55 +06)',
      sunset: '14:27 (20:27 +06)',
      uvi: 4.93,
      wind: 'E 5\u00a0mph'
    },
    {
      day: {
        feelsLike: '+15\u00a0°F',
        temp: '+19\u00a0°F'
      },
      dayStr: 'Mon, Apr 27',
      description: 'light rain',
      dt: moment(1587970800 * 1000),
      eve: {
        feelsLike: '+15\u00a0°F',
        temp: '+18\u00a0°F'
      },
      hourly: [],
      humidity: '42\u00a0%',
      icon: '10d',
      maxTemp: '+21\u00a0°F',
      minTemp: '+8\u00a0°F',
      morn: {
        feelsLike: '+4\u00a0°F',
        temp: '+8\u00a0°F'
      },
      night: {
        feelsLike: '+14\u00a0°F',
        temp: '+20\u00a0°F'
      },
      pressure: '1013\u00a0hPa/760\u00a0mmHg',
      sunrise: '23:53 (05:53 +06)',
      sunset: '14:28 (20:28 +06)',
      uvi: 5.66,
      wind: 'SSE 4\u00a0mph'
    },
    {
      day: {
        feelsLike: '+16\u00a0°F',
        temp: '+23\u00a0°F'
      },
      dayStr: 'Tue, Apr 28',
      description: 'light rain',
      dt: moment(1588057200 * 1000),
      eve: {
        feelsLike: '+16\u00a0°F',
        temp: '+20\u00a0°F'
      },
      hourly: [],
      humidity: '41\u00a0%',
      icon: '10d',
      maxTemp: '+23\u00a0°F',
      minTemp: '+16\u00a0°F',
      morn: {
        feelsLike: '+11\u00a0°F',
        temp: '+18\u00a0°F'
      },
      night: {
        feelsLike: '+11\u00a0°F',
        temp: '+16\u00a0°F'
      },
      pressure: '999\u00a0hPa/749\u00a0mmHg',
      sunrise: '23:52 (05:52 +06)',
      sunset: '14:30 (20:30 +06)',
      uvi: 5.97,
      wind: 'SW 10\u00a0mph'
    },
    {
      day: {
        feelsLike: '+6\u00a0°F',
        temp: '+14\u00a0°F'
      },
      dayStr: 'Wed, Apr 29',
      description: 'light rain',
      dt: moment(1588143600 * 1000),
      eve: {
        feelsLike: '+9\u00a0°F',
        temp: '+15\u00a0°F'
      },
      hourly: [],
      humidity: '56\u00a0%',
      icon: '10d',
      maxTemp: '+16\u00a0°F',
      minTemp: '+10\u00a0°F',
      morn: {
        feelsLike: '+6\u00a0°F',
        temp: '+12\u00a0°F'
      },
      night: {
        feelsLike: '+6\u00a0°F',
        temp: '+10\u00a0°F'
      },
      pressure: '1010\u00a0hPa/758\u00a0mmHg',
      sunrise: '23:50 (05:50 +06)',
      sunset: '14:32 (20:32 +06)',
      uvi: 5.24,
      wind: 'W 10\u00a0mph'
    },
    {
      day: {
        feelsLike: '+9\u00a0°F',
        temp: '+13\u00a0°F'
      },
      dayStr: 'Thu, Apr 30',
      description: 'few clouds',
      dt: moment(1588230000 * 1000),
      eve: {
        feelsLike: '+9\u00a0°F',
        temp: '+13\u00a0°F'
      },
      hourly: [],
      humidity: '37\u00a0%',
      icon: '02d',
      maxTemp: '+13\u00a0°F',
      minTemp: '+7\u00a0°F',
      morn: {
        feelsLike: '+4\u00a0°F',
        temp: '+7\u00a0°F'
      },
      night: {
        feelsLike: '+9\u00a0°F',
        temp: '+13\u00a0°F'
      },
      pressure: '1025\u00a0hPa/769\u00a0mmHg',
      sunrise: '23:48 (05:48 +06)',
      sunset: '14:33 (20:33 +06)',
      uvi: 5.37,
      wind: 'WNW 3\u00a0mph'
    }
  ]
};
