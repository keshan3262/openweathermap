import { groupBy, sortBy } from 'lodash';
import moment, { Moment } from 'moment-timezone';
import {
  formatPressure,
  formatTemperature,
  formatTime,
  formatWindDirection,
  formatWindSpeed
} from './format-weather-data';
import {
  BasicDayWeatherDescription,
  BasicWeatherDescription,
  CurrentWeatherDescription,
  Forecast,
  HourlyWeatherDescription,
  NonVerboseTemperatureDescription,
  Units,
  VerboseTemperatureDescription,
  DailyWeatherDescription,
  WeatherDescriptionItem
} from '../swagger/api';

export type TransformedBasicWeatherDescription = Pick<
  WeatherDescriptionItem,
  'description' | 'icon'
> & {
  dt: Moment;
  humidity: string;
  pressure: string;
  wind: string;
};

export type TransformedNonVerboseTemperatureDescription = {
  feelsLike: string;
  temp: string;
};

export type TransformedVerboseTemperatureDescription = {
  day: TransformedNonVerboseTemperatureDescription;
  eve: TransformedNonVerboseTemperatureDescription;
  maxTemp: string;
  minTemp: string;
  morn: TransformedNonVerboseTemperatureDescription;
  night: TransformedNonVerboseTemperatureDescription;
};

export type TransformedBasicDayWeatherDescription = TransformedBasicWeatherDescription & {
  sunrise: string;
  sunset: string;
  uvi: number;
};

export type TransformedCurrentWeatherDescription = TransformedBasicDayWeatherDescription &
  TransformedNonVerboseTemperatureDescription;

export type TransformedHourlyWeatherDescription = TransformedBasicWeatherDescription &
  TransformedNonVerboseTemperatureDescription;

export type TransformedDailyWeatherDescription = TransformedBasicDayWeatherDescription &
  TransformedVerboseTemperatureDescription;

export type TransformedForecastDay = TransformedDailyWeatherDescription & {
  dayStr: string;
  hourly: TransformedHourlyWeatherDescription[];
};

export type TransformedForecast = {
  current: TransformedCurrentWeatherDescription;
  days: TransformedForecastDay[];
};

const transformBasicWeatherDescription = (
  description: BasicWeatherDescription,
  units: Units
) => {
  const { dt, humidity, pressure, weather, wind_deg, wind_speed } = description;

  return {
    description: weather[0].description,
    dt: moment(dt * 1000),
    humidity: `${humidity}\u00a0%`,
    icon: weather[0].icon,
    pressure: formatPressure(pressure),
    wind: `${formatWindDirection(wind_deg)} ${formatWindSpeed(
      wind_speed,
      units
    )}`
  };
};

const transformNonVerboseTemperatureDescription = (
  { feels_like, temp }: NonVerboseTemperatureDescription,
  units: Units
) => {
  return {
    feelsLike: formatTemperature(feels_like, units),
    temp: formatTemperature(temp, units)
  };
};

const transformVerboseTemperatureDescription = (
  description: VerboseTemperatureDescription,
  units: Units
) => {
  const {
    temp: { day, eve, max, min, morn, night },
    feels_like: {
      day: feelsLikeDay,
      eve: feelsLikeEve,
      morn: feelsLikeMorn,
      night: feelsLikeNight
    }
  } = description;

  return {
    day: transformNonVerboseTemperatureDescription(
      {
        feels_like: feelsLikeDay,
        temp: day
      },
      units
    ),
    eve: transformNonVerboseTemperatureDescription(
      {
        feels_like: feelsLikeEve,
        temp: eve
      },
      units
    ),
    maxTemp: formatTemperature(max, units),
    minTemp: formatTemperature(min, units),
    morn: transformNonVerboseTemperatureDescription(
      {
        feels_like: feelsLikeMorn,
        temp: morn
      },
      units
    ),
    night: transformNonVerboseTemperatureDescription(
      {
        feels_like: feelsLikeNight,
        temp: night
      },
      units
    )
  };
};

const transformBasicDayWeatherDescription = (
  description: BasicDayWeatherDescription,
  units: Units,
  localTz: string
) => {
  const { sunrise, sunset, uvi, ...basicWeatherDescription } = description;

  return {
    ...transformBasicWeatherDescription(basicWeatherDescription, units),
    sunrise: formatTime(sunrise, localTz),
    sunset: formatTime(sunset, localTz),
    uvi
  };
};

const transformCurrentWeatherDescription = (
  description: CurrentWeatherDescription,
  units: Units,
  localTz: string
) => {
  const { feels_like, temp, ...basicDayWeatherDescription } = description;

  return {
    ...transformNonVerboseTemperatureDescription({ feels_like, temp }, units),
    ...transformBasicDayWeatherDescription(
      basicDayWeatherDescription,
      units,
      localTz
    )
  };
};

const transformHourlyWeatherDescription = (
  description: HourlyWeatherDescription,
  units: Units
) => {
  const { feels_like, temp, ...basicWeatherDescription } = description;

  return {
    ...transformNonVerboseTemperatureDescription({ feels_like, temp }, units),
    ...transformBasicWeatherDescription(basicWeatherDescription, units)
  };
};

const transformDailyWeatherDescription = (
  description: DailyWeatherDescription,
  units: Units,
  localTz: string
) => {
  const { feels_like, temp, ...basicDayWeatherDescription } = description;

  return {
    ...transformVerboseTemperatureDescription({ feels_like, temp }, units),
    ...transformBasicDayWeatherDescription(
      basicDayWeatherDescription,
      units,
      localTz
    )
  };
};

export const transformWeatherForecast = (
  forecast: Forecast
): TransformedForecast => {
  const { current, daily, hourly, timezone, units } = forecast;

  const transformedHourlyWeatherDescriptions = groupBy(
    hourly.map(description =>
      transformHourlyWeatherDescription(description, units)
    ),
    ({ dt }) => dt.format('ddd, MMM D')
  );

  const days = daily.map(description => {
    const transformedDescription = transformDailyWeatherDescription(
      description,
      units,
      timezone
    );

    const dayStr = transformedDescription.dt.format('ddd, MMM D');

    return {
      ...transformDailyWeatherDescription(description, units, timezone),
      dayStr,
      hourly: sortBy(transformedHourlyWeatherDescriptions[dayStr] || [], 'dt')
    };
  }, {});

  return {
    current: transformCurrentWeatherDescription(current, units, timezone),
    days
  };
};
