import { ConfigProps, reduxForm } from 'redux-form';
import withRedux from './with-redux';
import React from 'react';

const makeReduxFormDecorator = <FormData extends {}>(
  formConfig: Partial<ConfigProps<FormData>> & { form: string }
) => {
  return function ReduxFormDecorator(storyFn: () => React.ReactElement) {
    const Form = reduxForm(formConfig)(storyFn);

    return withRedux()(() => <Form form={formConfig.form} />);
  };
};

export default makeReduxFormDecorator;
