import Router from 'next/router';

/* eslint-disable-next-line immutable/no-mutation */
Router.router = Object.assign({}, Router.router, {
  push: () => Promise.resolve(true),
  prefetch: () => Promise.resolve()
});
