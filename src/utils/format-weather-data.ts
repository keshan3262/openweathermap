import convert from 'convert-units';
import { findKey } from 'lodash';
import moment from 'moment';
import {
  unitsLabels,
  windDirectionDegrees
} from '../constants/weather-forecast';
import { Units } from '../swagger/api';

export const formatPressure = (hPa: number) => {
  const mmHg = convert(hPa)
    .from('hPa')
    .to('torr');

  return `${hPa}\u00a0hPa/${Math.round(mmHg)}\u00a0mmHg`;
};

export const formatWindSpeed = (amount: number, units: Units) => {
  return `${Math.round(amount)}\u00a0${unitsLabels[units].speed}`;
};

export const formatWindDirection = (deg: number) => {
  return findKey(windDirectionDegrees, value =>
    value[1] > value[0]
      ? deg >= value[0] && deg < value[1]
      : deg >= value[0] || deg < value[1]
  );
};

export const formatTime = (timestamp: number, localTz: string) => {
  const browserMoment = moment(timestamp * 1000);
  const localMoment = browserMoment.clone().tz(localTz);

  const browserTime = browserMoment.format('HH:mm');
  const localTime = localMoment.format('HH:mm');

  if (browserTime === localTime) {
    return browserTime;
  }

  return `${browserTime} (${localTime} ${localMoment.format('z')})`;
};

export const formatTemperature = (amount: number, units: Units) => {
  return `${Math.round(amount) > 0 ? '+' : ''}${Math.round(amount)}\u00a0${
    unitsLabels[units].temperature
  }`;
};
