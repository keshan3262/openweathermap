import {
  rawImperialWeatherForecast,
  rawMetricWeatherForecast,
  transformedImperialWeatherForecast,
  transformedMetricWeatherForecast
} from '../test-data/weather-forecast';
import { transformWeatherForecast } from './transform-weather-forecast';

describe('utils/transformWeatherForecast', () => {
  it('should transform correctly raw weather forecast (metric units)', () =>
    expect(transformWeatherForecast(rawMetricWeatherForecast)).toEqual(
      transformedMetricWeatherForecast
    ));

  it('should transform correctly raw weather forecast (imperial units)', () =>
    expect(transformWeatherForecast(rawImperialWeatherForecast)).toEqual(
      transformedImperialWeatherForecast
    ));
});
