import * as Yup from 'yup';

export const makeValidateSyncFn = <T extends {}>(schema: Yup.ObjectSchema) => (
  values: T
) => {
  try {
    schema.validateSync(values, {
      abortEarly: false
    });

    return {};
  } catch (error) {
    const errors = (error as Yup.ValidationError).inner.reduce(
      (errorsPart, { message, path }) => {
        return {
          ...errorsPart,
          [path]: message
        };
      },
      {}
    );

    return errors;
  }
};
