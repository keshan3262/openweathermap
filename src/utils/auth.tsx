import cookie from 'js-cookie';
import { NextPage, NextPageContext } from 'next';
import nextCookie from 'next-cookies';
import Router from 'next/router';
import React, { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import {
  signIn as signInAction,
  signOut as signOutAction
} from '../redux/auth/actions';
import { UserData } from '../redux/auth/types';
import { LoginControllerApi } from '../swagger/api';

const loginControllerApi = new LoginControllerApi();

export const signIn = ({ token }: Record<string, string>) => {
  cookie.set('token', token, { expires: 7 });
  Router.push('/forecast');
};

export const redirectToSignIn = (ctx?: NextPageContext) => {
  if (ctx) {
    ctx.res?.writeHead(302, { Location: '/signin' });
    ctx.res?.end();
  } else {
    Router.push('/signin');
  }
};

export const getUserData = async (token?: string) => {
  if (!token) {
    return undefined;
  }

  try {
    const { email, nickname } = await loginControllerApi.apiUsersMeGet({
      headers: {
        Authorization: token
      }
    });

    return {
      email,
      nickname,
      token
    };
  } catch (error) {
    return undefined;
  }
};

export const auth = async (
  ctx: NextPageContext,
  authRequired = true
): Promise<UserData | undefined> => {
  const { token } = nextCookie(ctx);

  const userData = await getUserData(token);

  if (!userData && authRequired) {
    if (token) {
      signOut();
    } else {
      redirectToSignIn(ctx);
    }
  }

  return userData;
};

export const signOut = () => {
  cookie.remove('token');

  if (window != null) {
    window.localStorage.setItem('logout', String(Date.now()));
  }

  Router.push('/signin');
};

export const withAuthSync = <PageProps extends { userData?: UserData }>(
  authRequired = true
) => (WrappedComponent: NextPage<PageProps>) => {
  const Wrapper: NextPage<PageProps> = props => {
    const { userData } = props;

    const dispatch = useDispatch();

    useEffect(() => {
      if (userData) {
        dispatch(signInAction(userData));
      } else {
        dispatch(signOutAction());
      }
    }, [dispatch, userData]);

    const syncLogout = useCallback((event: StorageEvent) => {
      if (event.key === 'logout') {
        console.log('logged out from storage!');
        Router.push('/signin');
      }
    }, []);

    useEffect(() => {
      window.addEventListener('storage', syncLogout);

      return () => {
        window.removeEventListener('storage', syncLogout);
        window.localStorage.removeItem('logout');
      };
    }, [syncLogout]);

    return <WrappedComponent {...props} />;
  };

  /* eslint-disable-next-line immutable/no-mutation */
  Wrapper.getInitialProps = async (ctx: NextPageContext) => {
    const userData = await auth(ctx, authRequired);

    const componentProps = (WrappedComponent.getInitialProps &&
      (await WrappedComponent.getInitialProps(ctx))) as PageProps;

    return { ...componentProps, userData };
  };

  return Wrapper;
};
