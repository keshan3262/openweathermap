import React from 'react';
import { Provider } from 'react-redux';
import { initialState as defaultApiInitialState } from '../redux/api/reducers';
import { initialState as defaultAuthInitialState } from '../redux/auth/reducers';
import { initialState as defaultCitiesInitialState } from '../redux/cities/reducers';
import { initialState as defaultErrorInitialState } from '../redux/error/reducers';
import { initialState as defaultForecastInitialState } from '../redux/forecast/reducers';
import makeStore, { RootState } from '../redux/store';

const defaultInitialState = {
  api: defaultApiInitialState,
  auth: defaultAuthInitialState,
  cities: defaultCitiesInitialState,
  error: defaultErrorInitialState,
  forecast: defaultForecastInitialState,
  form: {}
};

const withRedux = (initialStateOverrides?: Partial<RootState>) => {
  const store = makeStore({
    ...defaultInitialState,
    ...initialStateOverrides
  });

  return function WithRedux(fn: () => React.ReactElement) {
    return <Provider store={store}>{fn()}</Provider>;
  };
};

export default withRedux;
