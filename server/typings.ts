import { RequestHandler } from 'express';

export type RequestHandlerParams = RequestHandler | RequestHandler[];
