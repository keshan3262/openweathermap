import { checkSchema, oneOf, ParamSchema } from 'express-validator';
import { omit, pickBy } from 'lodash';
import fetch, { FetchError } from 'node-fetch';
import { format as formatUrl } from 'url';
import { auth } from '../middleware/auth';
import { sendExpressValidationErrors } from '../middleware/validation';
import { City } from '../models/city';
import { RequestHandlerParams } from '../typings';

type ForecastParams = {
  cityId?: number;
  lang?: string;
  lat?: number;
  lon?: number;
  units?: 'imperial' | 'metric';
};

const locationByCoordinatesSchema: Record<string, ParamSchema> = {
  lat: {
    in: 'query',
    isFloat: {
      options: {
        max: 90,
        min: -90
      }
    }
  },
  lon: {
    in: 'query',
    isFloat: {
      options: {
        max: 180,
        min: -180
      }
    }
  }
};

const locationByCitySchema: Record<string, ParamSchema> = {
  cityId: {
    in: 'query',
    isInt: {
      options: {
        min: 1
      }
    }
  }
};

const restForecastParamsSchema: Record<string, ParamSchema> = {
  lang: {
    in: 'query',
    isIn: {
      options: [
        [
          'af',
          'al',
          'ar',
          'az',
          'bg',
          'ca',
          'cz',
          'da',
          'de',
          'el',
          'en',
          'eu',
          'fa',
          'fi',
          'fr',
          'gl',
          'he',
          'hi',
          'hr',
          'hu',
          'id',
          'it',
          'ja',
          'kr',
          'la',
          'lt',
          'mk',
          'no',
          'nl',
          'pl',
          'pt',
          'pt_br',
          'ro',
          'ru',
          'sv',
          'se',
          'sk',
          'sl',
          'sp',
          'es',
          'sr',
          'th',
          'tr',
          'ua',
          'uk',
          'vi',
          'zh_cn',
          'zh_tw',
          'zu'
        ]
      ]
    },
    optional: true
  },
  units: {
    in: 'query',
    isIn: {
      options: [['metric', 'imperial']]
    },
    optional: true
  }
};

export const getForecastHandlers: RequestHandlerParams[] = [
  auth,
  oneOf([
    checkSchema(locationByCoordinatesSchema),
    checkSchema(locationByCitySchema)
  ]),
  checkSchema(restForecastParamsSchema),
  sendExpressValidationErrors,
  async (req, res) => {
    const { cityId, lang, lat, lon, units } = req.query as ForecastParams;

    let location = lat ? { lat, lon: lon! } : { lat: 0, lon: 0 };

    try {
      const appid = process.env.OPENWEATHERMAP_API_TOKEN;

      if (!lat) {
        const city = await City.findOne({ id: cityId });

        if (!city) {
          return res.header(404).json('No such city was found');
        }

        location = {
          lat: city.location.coordinates[1],
          lon: city.location.coordinates[0]
        };
      }

      const response = await fetch(
        formatUrl({
          host: 'https://api.openweathermap.org',
          pathname: '/data/2.5/onecall',
          query: pickBy(
            {
              appid,
              lat: location.lat,
              lon: location.lon,
              lang,
              units
            },
            value => value != null
          )
        })
      );

      const forecast = await response.json();

      return res.json({
        ...omit(forecast, 'lat', 'lon'),
        units
      });
    } catch (error) {
      if (error.name === 'FetchError') {
        const { code = '500', message } = error as FetchError;

        return res.status(Number(code)).json(message);
      }

      return res.status(500).send(error.message);
    }
  }
];
