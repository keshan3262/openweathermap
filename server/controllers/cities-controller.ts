import { checkSchema, ParamSchema } from 'express-validator';
import { pickBy, uniqBy } from 'lodash';
import { City } from '../models/city';
import { RequestHandlerParams } from '../typings';
import { auth } from '../middleware/auth';
import { sendExpressValidationErrors } from '../middleware/validation';

export type CitiesFilter = {
  country?: string;
  name?: string;
  page?: number;
};

const escapeRegExp = (str: string) => {
  return str.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
};

const citiesFilterSchema: Record<string, ParamSchema> = {
  country: {
    in: 'query',
    isString: true,
    optional: true
  },
  name: {
    in: 'query',
    isString: true,
    optional: true
  },
  page: {
    in: 'query',
    isInt: {
      options: {
        min: 1
      }
    },
    optional: true
  }
};

const pageSize = 100;

export const getCitiesHandlers: RequestHandlerParams[] = [
  auth,
  checkSchema(citiesFilterSchema),
  sendExpressValidationErrors,
  async (req, res) => {
    const {
      country: searchCountry = '',
      name,
      page = 1
    } = req.query as CitiesFilter;

    try {
      const textMatchingCities = await City.find(
        pickBy(
          {
            $text: name ? { $search: name } : undefined,
            country: searchCountry
          },
          value => value !== undefined
        )
      )
        .limit(pageSize)
        .skip((page - 1) * pageSize);

      const startsWithMatchingCities = await City.find(
        pickBy(
          {
            country: searchCountry,
            name: name
              ? { $regex: new RegExp(`^${escapeRegExp(name)}`) }
              : undefined
          },
          value => value !== undefined
        )
      )
        .limit(pageSize)
        .skip((page - 1) * pageSize);

      const cities = uniqBy(
        [...startsWithMatchingCities, ...textMatchingCities],
        'id'
      ).slice(0, 100);

      return res.json(
        cities.map(({ country, id, location, name, state }) => ({
          country,
          id,
          location: {
            lat: location.coordinates[1],
            lng: location.coordinates[0]
          },
          name,
          state
        }))
      );
    } catch (error) {
      return res.status(500).json(error.message);
    }
  }
];
