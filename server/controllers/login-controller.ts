import { compare, hash } from 'bcryptjs';
import { checkSchema, ParamSchema } from 'express-validator';
import { pick } from 'lodash';
import { sign } from 'jsonwebtoken';
import { User, UserDocument } from '../models/user';
import { RequestHandlerParams } from '../typings';
import { auth } from '../middleware/auth';
import { sendExpressValidationErrors } from '../middleware/validation';

export type SignInCredentials = {
  email: string;
  password: string;
};

export type SignUpCredentials = {
  email: string;
  nickname: string;
  password: string;
};

const addUserToken = async (user: UserDocument) => {
  const { _id, email, nickname } = user;

  const token = sign({ _id, email, nickname }, process.env.JWT_KEY!);

  const newUser = await User.findByIdAndUpdate(
    _id,
    { $push: { tokens: { token } } },
    { new: true }
  );

  return {
    token,
    user: newUser
  };
};

const signInCredentialsSchema: Record<string, ParamSchema> = {
  email: {
    errorMessage: 'email should have valid format',
    in: 'body',
    isEmail: true
  },
  password: {
    in: 'body',
    isString: true
  }
};

const signUpCredentialsSchema: Record<string, ParamSchema> = {
  email: {
    errorMessage: 'email should have valid format',
    in: 'body',
    isEmail: true
  },
  password: {
    errorMessage: `password should have from 8 to 30 characters, only alphanumeric \
and !@#$%^&* characters are allowed`,
    matches: {
      options: /[A-z0-9!@#$%^&*]{8,30}/
    }
  },
  nickname: {
    in: 'body',
    isString: true,
    isLength: {
      options: { max: 32, min: 3 }
    }
  }
};

export const signUpHandlers: RequestHandlerParams[] = [
  checkSchema(signUpCredentialsSchema),
  sendExpressValidationErrors,
  async (req, res) => {
    const { email, nickname, password } = req.body as SignUpCredentials;

    try {
      const passwordHash = await hash(password, 8);

      const newUser = new User({
        email,
        nickname,
        passwordHash,
        tokens: []
      });

      const { _id: newUserId } = await newUser.save();
      const { token, user: newUserDoc } = await addUserToken(newUserId);

      return res.header('Authorization', `Bearer ${token}`).json({
        user: pick(newUserDoc, '_id', 'email', 'nickname'),
        token: `Bearer ${token}`
      });
    } catch (error) {
      return res.status(500).json(error.message);
    }
  }
];

export const signInHandlers: RequestHandlerParams[] = [
  checkSchema(signInCredentialsSchema),
  sendExpressValidationErrors,
  async (req, res) => {
    const { email, password } = req.body as SignUpCredentials;

    try {
      const user = await User.findOne({ email });

      if (user && (await compare(password, user.passwordHash))) {
        const { token, user: newUserDoc } = await addUserToken(user._id);

        return res.header('Authorization', `Bearer ${token}`).json({
          user: pick(newUserDoc, '_id', 'email', 'nickname'),
          token: `Bearer ${token}`
        });
      }

      throw { message: 'Invalid login credentials' };
    } catch (error) {
      if (error instanceof Error) {
        return res.status(500).json(error.message);
      }

      return res.status(401).json(error.message);
    }
  }
];

export const aboutUserHandlers: RequestHandlerParams[] = [
  auth,
  async (req, res) =>
    res.json(pick(res.locals.user, '_id', 'email', 'nickname'))
];
