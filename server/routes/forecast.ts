import { getForecastHandlers } from '../controllers/forecast-controller';
import { Router } from 'express';

export default (router: Router) => {
  /**
   * @swagger
   * /api/forecast:
   *  get:
   *    security:
   *      - BearerAuth: []
   *    description: Get weather forecast by city id or point coordinates
   *    tags:
   *      - forecast-controller
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: cityId
   *        in: query
   *        schema:
   *          type: number
   *      - name: lang
   *        in: query
   *        schema:
   *          $ref: '#/definitions/Language'
   *      - name: lat
   *        in: query
   *        schema:
   *          type: number
   *      - name: lon
   *        in: query
   *        schema:
   *          type: number
   *      - name: units
   *        in: query
   *        schema:
   *          $ref: '#/definitions/Units'
   *    responses:
   *      200:
   *        schema:
   *          $ref: '#/definitions/Forecast'
   *      400:
   *        schema:
   *          type: string
   *      401:
   *        schema:
   *          type: string
   *      404:
   *        schema:
   *          type: string
   *      409:
   *        schema:
   *          type: string
   *      500:
   *        schema:
   *          type: string
   */
  router.route('/api/forecast').get(...getForecastHandlers);
};
