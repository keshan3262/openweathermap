import { Router } from 'express';
import setupCitiesRoutes from './cities';
import setupForecastRoutes from './forecast';
import setupLoginRoutes from './login';

const router = Router();

setupCitiesRoutes(router);
setupForecastRoutes(router);
setupLoginRoutes(router);

export default router;
