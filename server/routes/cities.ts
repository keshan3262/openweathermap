import { getCitiesHandlers } from '../controllers/cities-controller';
import { Router } from 'express';

export default (router: Router) => {
  /**
   * @swagger
   * /api/cities:
   *  get:
   *    security:
   *      - BearerAuth: []
   *    description: Filter cities by country and name
   *    tags:
   *      - cities-controller
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: country
   *        in: query
   *        schema:
   *          type: string
   *      - name: name
   *        in: query
   *        schema:
   *          type: string
   *      - name: page
   *        in: query
   *        schema:
   *          type: number
   *    responses:
   *      200:
   *        schema:
   *          type: array
   *          items:
   *            $ref: '#/definitions/City'
   *      400:
   *        schema:
   *          type: string
   *      401:
   *        schema:
   *          type: string
   *      500:
   *        schema:
   *          type: string
   */
  router.route('/api/cities').get(...getCitiesHandlers);
};
