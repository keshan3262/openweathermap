import {
  signInHandlers,
  signUpHandlers,
  aboutUserHandlers
} from '../controllers/login-controller';
import { Router } from 'express';

/**
 * @swagger
 * definitions:
 *  ValidationError:
 *    type: object
 *    required:
 *      - msg
 *      - param
 *      - location
 *    properties:
 *      msg:
 *        type: string
 *      param:
 *        type: string
 *      location:
 *        type: string
 *  ValidationErrors:
 *    type: object
 *    required:
 *      - errors
 *    properties:
 *      errors:
 *        type: array
 *        items:
 *          $ref: '#/definitions/ValidationError'
 */

export default (router: Router) => {
  /**
   * @swagger
   * /api/users/signin:
   *  post:
   *    description: Sign in to API by username and password
   *    tags:
   *      - login-controller
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: credentials
   *        description: User's credentials
   *        in: body
   *        required: true
   *        schema:
   *          $ref: '#/definitions/SignInCredentials'
   *    responses:
   *      200:
   *        headers:
   *          Authorization:
   *            schema:
   *              type: string
   *        schema:
   *          $ref: '#/definitions/LoginResponse'
   *      400:
   *        schema:
   *          $ref: '#/definitions/ValidationErrors'
   *      401:
   *        schema:
   *          type: string
   *      500:
   *        schema:
   *          type: string
   */
  router.route('/api/users/signin').post(...signInHandlers);
  /**
   * @swagger
   * /api/users/signup:
   *  post:
   *    description: Create a new user and sign in to API
   *    tags:
   *      - login-controller
   *    produces:
   *      - application/json
   *    parameters:
   *      - name: credentials
   *        description: User's credentials
   *        in: body
   *        required: true
   *        schema:
   *          $ref: '#/definitions/NewUser'
   *    responses:
   *      200:
   *        headers:
   *          Authorization:
   *            schema:
   *              type: string
   *        schema:
   *          $ref: '#/definitions/LoginResponse'
   *      400:
   *        schema:
   *          $ref: '#/definitions/ValidationErrors'
   *      500:
   *        schema:
   *          type: string
   */
  router.route('/api/users/signup').post(...signUpHandlers);
  /**
   * @swagger
   * /api/users/me:
   *  get:
   *    security:
   *      - BearerAuth: []
   *    description: Get data for user by auth token from header
   *    tags:
   *      - login-controller
   *    produces:
   *      - application/json
   *    responses:
   *      200:
   *        schema:
   *          $ref: '#/definitions/User'
   *      400:
   *        schema:
   *          type: string
   *      401:
   *        schema:
   *          type: string
   */
  router.route('/api/users/me').get(...aboutUserHandlers);
};
