import { verify } from 'jsonwebtoken';
import { User } from '../models/user';
import { RequestHandler } from 'express';

type TokenData = {
  _id: string;
};

/**
 * @swagger
 * securityDefinitions:
 *   BearerAuth:
 *     type: http
 *     scheme: bearer
 *     bearerFormat: JWT
 */

export const validateAuthHeader = async (header: string) => {
  const token = header.replace('Bearer ', '');

  const data = verify(token, process.env.JWT_KEY!) as TokenData;

  const user = await User.findOne({ _id: data._id, 'tokens.token': token });

  if (!user) {
    throw new Error('Invalid authorization');
  }

  return { token, user };
};

export const auth: RequestHandler = async (req, res, next) => {
  const header = req.header('Authorization');

  if (!header) {
    res.status(401).json('Authorization required');

    return;
  }

  try {
    const { token, user } = await validateAuthHeader(header);

    /* eslint-disable immutable/no-mutation */
    res.locals.user = user;
    res.locals.token = token;
    next();
    /* eslint-enable immutable/no-mutation */
  } catch (error) {
    res.status(401).json('Invalid authorization');
  }
};
