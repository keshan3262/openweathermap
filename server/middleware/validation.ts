import { validationResult } from 'express-validator';
import { RequestHandler } from 'express';

export const sendExpressValidationErrors: RequestHandler = (req, res, next) => {
  const validationErrors = validationResult(req);

  if (!validationErrors.isEmpty()) {
    return res.status(400).json({ errors: validationErrors.array() });
  }

  return next();
};
