import { Document, model, Schema } from 'mongoose';

/**
 * @swagger
 * definitions:
 *  Location:
 *    type: object
 *    required:
 *      - lat
 *      - lng
 *    properties:
 *      lat:
 *        type: number
 *      lng:
 *        type: number
 *  City:
 *    type: object
 *    required:
 *      - country
 *      - id
 *      - location
 *      - name
 *      - state
 *    properties:
 *      country:
 *        type: string
 *      id:
 *        type: number
 *      location:
 *        $ref: '#/definitions/Location'
 *      name:
 *        type: string
 *      state:
 *        type: string
 */
export interface CityDocument extends Document {
  country: string;
  id: number;
  location: {
    coordinates: [number, number];
    type: 'Point';
  };
  name: string;
  state: string;
}

export const CitySchema = new Schema({
  country: {
    default: '',
    index: true,
    type: String
  },
  id: {
    required: true,
    type: Number,
    unique: true
  },
  location: {
    coordinates: {
      type: [Number],
      required: true
    },
    type: {
      enum: ['Point'],
      required: true,
      type: String
    }
  },
  name: {
    required: true,
    text: true,
    type: String
  },
  state: {
    default: '',
    type: String
  }
});
CitySchema.index({ name: 1 });

export const City = model<CityDocument>('City', CitySchema);
