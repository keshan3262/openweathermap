import { Document, model, Schema } from 'mongoose';

/**
 * @swagger
 * definitions:
 *  NewUser:
 *    type: object
 *    required:
 *      - email
 *      - nickname
 *      - password
 *    properties:
 *      email:
 *        type: string
 *        format: email
 *      nickname:
 *        type: string
 *      password:
 *        type: string
 *        format: password
 *  SignInCredentials:
 *    type: object
 *    required:
 *      - email
 *      - password
 *    properties:
 *      email:
 *        type: string
 *        format: email
 *      password:
 *        type: string
 *        format: password
 *  User:
 *    type: object
 *    required:
 *      - _id
 *      - email
 *      - nickname
 *    properties:
 *      _id:
 *        type: string
 *      email:
 *        type: string
 *      nickname:
 *        type: string
 *  LoginResponse:
 *    type: object
 *    required:
 *      - token
 *      - user
 *    properties:
 *      token:
 *        type: string
 *      user:
 *        $ref: '#/definitions/User'
 */

export interface UserDocument extends Document {
  email: string;
  nickname: string;
  passwordHash: string;
  tokens: Array<{
    token: string;
  }>;
}

export const UserSchema = new Schema({
  email: {
    required: true,
    type: String,
    unique: true
  },
  nickname: {
    required: true,
    type: String
  },
  passwordHash: {
    required: true,
    type: String
  },
  tokens: [
    {
      token: {
        index: true,
        required: true,
        type: String
      }
    }
  ]
});

export const User = model<UserDocument>('User', UserSchema);
