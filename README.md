# openweathermap

This is a MERN stack web application which provides weather forecasts. TypeScript is used in both frontend and backend parts.

## Technologies

Code quality control:
- [jscpd](https://github.com/kucherenko/jscpd)
- [ESLint](https://eslint.org)

Backend part:
- [Express](http://expressjs.com/)
- [Mongoose](https://mongoosejs.com/)
- [Swagger](https://swagger.io/)
- [socket.io](https://socket.io)

Frontend part:
- [Material UI](https://material-ui.com)
- [React](https://reactjs.org/)
- [Next.js](https://nextjs.org/)
- [Redux](http://redux.js.org/)
- [Swagger](https://swagger.io/)
- [Socket.io-client](https://github.com/socketio/socket.io-client)
- Test modules:
  * [Storybook](https://storybook.js.org/)
  * [Jest](https://jestjs.io/)
  * [Puppeteer](https://github.com/puppeteer/puppeteer)

## Setup

These lines install all necessary modules and creates MongoDB database with collection of all cities from [OpenWeather](https://openweathermap.org)

```
npm install
npm run import-cities
```

Also you should insert your own OpenWeatherMap API key (`OPENWEATHERMAP_API_TOKEN` variable) into .env and replace JWT_KEY there with one of your choice. 

## Scripts

### `npm run dev`

Starts the application in development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Creates a production build of the application. Needs to be run before running the application in production mode.

### `npm run import-cities`

Ensures that `openweathermap` database with collection of cities is available in MongoDB.

### `npm run jscpd`

Checks source code for copypaste.

### `npm run start`

Starts the application in production mode.

### `npm run storybook`

Runs the storybook.

Open [http://localhost:6006](http://localhost:6006) to view it in the browser.

The page will reload if you make edits.

### `npm run storybook:build`

Builds the storybook for production to `storybook-static` folder.

### `npm run storybook:build:storyshot`

Builds storybook for image snapshots testing.

### `npm run test:unit:storyshot`

Runs Jest snapshot tests.

### `npm run test:unit:storyshot:image:server`

Runs the storybook server which provides storybook from storybook-static directory.

Open [http://localhost:6006](http://localhost:6006) to view it in the browser.

### `npm run test:unit:storyshot:image`

Runs image storyshot tests. `npm run test:unit:storyshot:image:server` needs to be running at the background.

### `npm run swagger:check`

Checks if swagger specification from `swagger/api-docs.json` is up to date. Throws error if it's false.

### `npm run swagger:update`

Downloads the newest swagger specification from API server and generates typescript code which enables working with the API.

For now it uses swagger-codegen-cli.jar file, so you need to have JRE installed.
